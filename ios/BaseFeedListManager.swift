//
//  BaseFeedListManager.swift
//
//  Created by Maxim Soloviev on 29/01/16.
//

import Foundation
import SwiftyJSON
import ORCoreData

class BaseFeedListManager<T: UpdateableEntity> : BaseManager {

    typealias FeedListResponseWithErrorBlock = (objects: [T], sessionWasRecreated: Bool, fullyLoaded: Bool, error: NSError?) -> Void

    private var feedListName = ""
    
    var lastObjectIndex = 0
    
    var queryInProgress = false
    
    var paginationEnabled = true
    
    init(feedListName: String, paginationEnabled: Bool = true) {
        self.feedListName = feedListName
        self.paginationEnabled = paginationEnabled
    }

    private override init() {
        super.init()
    }
    
    private var feedList: FeedList {
        return FeedList.findOrCreateFeedListWithName(feedListName, inContext: defaultContext)
    }
    
    func reloadFeedWithRequestParameters(params: RequestParameters?, limit: Int, completion: FeedListResponseWithErrorBlock) {
        if queryInProgress {
            self.callCompletion(completion, objects: [], sessionWasRecreated: false, fullyLoaded: false, error: AppError.errorWithCode(.AppErrorFeedListQueryInProgress))
            return
        }
        queryInProgress = true

        getObjectsFromServer(requestParameters: params, limit: limit) { (json, error) -> Void in
            if error != nil {
                self.callCompletion(completion, objects: [], sessionWasRecreated: false, fullyLoaded:false, error: error)
                return
            }
            
            guard let jsonResponse = json else {
                self.callCompletion(completion, objects: [], sessionWasRecreated: false, fullyLoaded:false, error: AppError.errorWithCode(.AppErrorEmptyJSON))
                return
            }
            
            self.saveDataFromJSON(jsonResponse, limit: limit, skip: 0, removeOldObjects: true, completion: completion)
        }
    }
    
    func getFirstPage(completion: FeedListResponseWithErrorBlock) {
        if queryInProgress {
            self.callCompletion(completion, objects: [], sessionWasRecreated: false, fullyLoaded: false, error: AppError.errorWithCode(.AppErrorFeedListQueryInProgress))
            return
        }
        queryInProgress = true
        
        getObjects(skip: 0, completion: completion)
    }
    
    func getNextPage(completion: FeedListResponseWithErrorBlock) {
        if !paginationEnabled {
            fatalError("getNextPage only available when pagination is enabled!")
        }
        
        if queryInProgress {
            self.callCompletion(completion, objects: [], sessionWasRecreated: false, fullyLoaded: false, error: AppError.errorWithCode(.AppErrorFeedListQueryInProgress))
            return
        }
        queryInProgress = true
        
        getObjects(skip: lastObjectIndex, completion: completion)
    }

    private func getObjects(skip skip: Int, completion: FeedListResponseWithErrorBlock) {
        let feedList = self.feedList
        
        var sessionId: String?
        
        if paginationEnabled {
            guard feedList.sessionId != nil, let validUntil = feedList.validUntil else {
                self.callCompletion(completion, objects: [], sessionWasRecreated: false, fullyLoaded:false, error: AppError.errorWithCode(.AppErrorNeedToCreateNewSession))
                return
            }
            
            sessionId = feedList.sessionId

            let sessionIsExpired = validUntil.compare(NSDate().toUTC()) != .OrderedDescending
            if sessionIsExpired {
                self.callCompletion(completion, objects: [], sessionWasRecreated: false, fullyLoaded:false, error: AppError.errorWithCode(.AppErrorNeedToCreateNewSession))
                return
            }
        }

        let currentPageSize = feedList.currentPageSize!.integerValue
        var needToRetrieveDataFromServer = !feedList.fullyLoaded!.boolValue
        if needToRetrieveDataFromServer {
            needToRetrieveDataFromServer = feedList.objects!.count < (currentPageSize + skip)
        }
        
        if needToRetrieveDataFromServer {
            // get objects from server
            getObjectsFromServer(sessionId: sessionId, limit: currentPageSize, skip: skip) { (json, error) -> Void in
                if error != nil {
                    self.callCompletion(completion, objects: [], sessionWasRecreated: false, fullyLoaded:false, error: error)
                    return
                }
                
                guard let jsonResponse = json else {
                    self.callCompletion(completion, objects: [], sessionWasRecreated: false, fullyLoaded:false, error: AppError.errorWithCode(.AppErrorEmptyJSON))
                    return
                }
                
                self.saveDataFromJSON(jsonResponse, limit: currentPageSize, skip: skip, removeOldObjects: false, completion: completion)
            }
        } else {
            // get local objects
            var objects = [T]()
            var fullyLoaded = false
            lastObjectIndex = min(skip + currentPageSize, feedList.objects!.count)
            if lastObjectIndex == feedList.objects!.count {
                fullyLoaded = true
            }
            
            for i in skip ..< lastObjectIndex {
                let obj = feedList.objects![i] as! T
                objects.append(obj)
            }
            self.callCompletion(completion, objects: objects, sessionWasRecreated: false, fullyLoaded: fullyLoaded, error: nil)
        }
    }
    
    private func saveDataFromJSON(jsonResponse: JSON, limit: Int, skip: Int, removeOldObjects: Bool, completion: FeedListResponseWithErrorBlock) {
        var objectIds = [ObjectIdType]()

        ORCoreDataSaver.sharedInstance.saveData({ [weak self] (localContext, cancelSaving) -> Void in
            if let strongSelf = self {
                do {
                    // get feed list in local context
                    let localContextFeedList = FeedList.findOrCreateFeedListWithName(strongSelf.feedListName, inContext: localContext)
                    localContextFeedList.currentPageSize = limit
                    
                    let jsonObjects: [JSON]
                    if strongSelf.paginationEnabled {
                        // get feed list parameters
                        let feedListParameters = try FeedListParser.jsonToFeedListParameters(jsonResponse)
                        let sessionIdFromResponse = feedListParameters.sessionId
                        localContextFeedList.sessionId = sessionIdFromResponse
                        localContextFeedList.validUntil = feedListParameters.validUntil
                        jsonObjects = feedListParameters.jsonObjects
                    } else {
                        jsonObjects = jsonResponse.arrayValue
                    }
                    
                    let objects = removeOldObjects ? NSMutableOrderedSet() : NSMutableOrderedSet(orderedSet: localContextFeedList.objects!)
                    let entityFinderAndCreator = EntityFinderAndCreator(localContext)
                    
                    // parse and save objects
                    for jsonObject in jsonObjects {
                        let object = try strongSelf.jsonToObject(jsonObject, entityFinderAndCreator: entityFinderAndCreator)
                        
                        if objects.containsObject(object) {
                            // objects must not be duplicated in FeedList!
                            cancelSaving = true
                            dispatch_async(dispatch_get_main_queue(), {
                                strongSelf.callCompletion(completion, objects: [], sessionWasRecreated: false, fullyLoaded: false, error: AppError.errorWithCode(.AppErrorDuplicatedItemsInFeedList))
                            })
                            return
                        }
                        
                        objectIds.append(object.uid!)
                        objects.addObject(object)
                    }
                    localContextFeedList.objects = objects
                    
                    // check if feed list have all objects from server (objects count from response < limit)
                    let newObjectCount = objectIds.count
                    if newObjectCount < limit {
                        localContextFeedList.fullyLoaded = true
                    }
                } catch {
                    cancelSaving = true
                    print("FeedList parsing: invalid object!")
                    dispatch_async(dispatch_get_main_queue(), {
                        strongSelf.callCompletion(completion, objects: [], sessionWasRecreated: false, fullyLoaded: false, error: AppError.errorWithCode(.AppErrorInvalidJSON))
                    })
                }
            }
        }, success: { [weak self] () -> Void in
            if let strongSelf = self {
                if removeOldObjects {
                    strongSelf.lastObjectIndex = 0
                }
                strongSelf.lastObjectIndex = skip + objectIds.count
            }
            
            dispatch_async(dispatch_get_main_queue(), { [weak self] in
                if let strongSelf = self {
                    if !objectIds.isEmpty {
                        // push all objects received from server to array and pass to completion
                        var objects = [T]()
                        for uid in objectIds {
                            if let object = EntityFinderAndCreator(strongSelf.defaultContext).findEntityOfType(T.self, withId: uid) {
                                objects.append(object)
                            } else {
                                strongSelf.callCompletion(completion, objects: [], sessionWasRecreated: false, fullyLoaded: false, error: AppError.errorWithCode(.AppErrorCoreDataFetch))
                                return
                            }
                        }
                        strongSelf.callCompletion(completion, objects: objects, sessionWasRecreated: removeOldObjects, fullyLoaded: strongSelf.feedList.fullyLoaded!.boolValue, error: nil)
                    } else {
                        strongSelf.callCompletion(completion, objects: [], sessionWasRecreated: false, fullyLoaded: true, error: nil)
                    }
                }
            })
        })
    }

    func callCompletion(completion: FeedListResponseWithErrorBlock, objects: [T], sessionWasRecreated: Bool, fullyLoaded: Bool, error: NSError?) {
        queryInProgress = false
        completion(objects: objects, sessionWasRecreated: sessionWasRecreated, fullyLoaded: fullyLoaded, error: error)
    }
    
    func getObjectsFromServer(requestParameters requestParameters: RequestParameters?, limit: Int?, completion: JSONResponseBlock) {
        fatalError("Must be overridden!")
    }

    func getObjectsFromServer(sessionId sessionId: ObjectIdType?, limit: Int?, skip: Int?, completion: JSONResponseBlock) {
        fatalError("Must be overridden!")
    }
    
    func jsonToObject(json: JSON, entityFinderAndCreator: EntityFinderAndCreator) throws -> T {
        fatalError("Must be overridden!")
    }
}

DynamicCollectionViewLayout (Swift) - custom layout for collection view with the use of  uikit dynamics

ORCoreDataSaver (Swift) - saving  coredata objects in one queue in order to avoid duplicating entities  when saving in parallel.

UIScrollView+KeyboardInsetHandler (Swift) - helper for automatic contentInset install in scroll view  when keyboard is displayed on screen

Clue (Swift) is a sample of module with viper-architecture.

BaseFeedListManager (Swift) - sample of data manager for viper.

SwitchMovieGrid (Objective-C) is a sample project which demonstrates the use of UICollectionView by displaying a list of movies obtained by making a request to a server. Each movie item can be viewed on a separate detailed screen.

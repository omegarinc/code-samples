//
//  MovieCollectionViewCell.m
//  SwitchMovieGrid
//
//  Created by Alexander Kurbanov on 26.11.15.
//  Copyright © 2015 Alexander Kurbanov. All rights reserved.
//

#import "MovieCollectionViewCell.h"

@implementation MovieCollectionViewCell

#pragma mark - View lifecycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.layer.cornerRadius = 5;
}

@end

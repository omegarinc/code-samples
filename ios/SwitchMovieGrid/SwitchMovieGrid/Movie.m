//
//  Movie.m
//  SwitchMovieGrid
//
//  Created by Alexander Kurbanov on 26.11.15.
//  Copyright © 2015 Alexander Kurbanov. All rights reserved.
//

#import "Movie.h"

@implementation Movie

- (NSURL *)urlForPoster
{
    const int width = 342;
    NSString const *baseURL = @"http://image.tmdb.org/t/p/";
    NSString *result = [baseURL stringByAppendingFormat:@"w%i%@", width, self.posterPath];
    return [NSURL URLWithString:result];
}

@end

//
//  MovieManager.m
//  SwitchMovieGrid
//
//  Created by Alexander Kurbanov on 26.11.15.
//  Copyright © 2015 Alexander Kurbanov. All rights reserved.
//

#define API_KEY @"ebea8cfca72fdff8d2624ad7bbf78e4c"

#import "Movie.h"
#import "MovieManager.h"

@interface MovieManager ()

@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@end

@implementation MovieManager

#pragma mark - Object lifecycle

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.dateFormatter = [[NSDateFormatter alloc] init];
        [self.dateFormatter setDateFormat:@"yyyy-MM-dd"];
    }
    return self;
}

#pragma mark - Request methods

- (void)loadMoviesAtPage:(int)page completion:(ArrayResultBlock)completion
{
    __weak typeof(self) weakSelf = self;
    [[[NSURLSession sharedSession] dataTaskWithURL:[self urlForMethod:@"now_playing" parameters:@{@"page": [@(page) stringValue]}]
                                 completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                     __strong typeof (self) strongSelf = weakSelf;
                                     if (!strongSelf) {
                                         return;
                                     }
                                     if (error || !data) {
                                         completion(nil, error);
                                     } else {
                                         NSError *error = nil;
                                         NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                                              options:kNilOptions
                                                                                                error:&error];
                                         if (error) {
                                             completion(nil, error);
                                         } else {
                                             NSArray *objects = json[@"results"];
                                             NSMutableArray *movies = [NSMutableArray array];
                                             for (NSDictionary *dict in objects) {
                                                 Movie *movie = [strongSelf movieFromDictionary:dict];
                                                 if (movie) {
                                                     [movies addObject:movie];
                                                 }
                                             }
                                             completion([movies copy], nil);
                                         }
                                         
                                     }
                                 }] resume];
}


#pragma mark - Parsing methods

- (Movie *)movieFromDictionary:(NSDictionary *)dict
{
    Movie *movie = [[Movie alloc] init];
    movie.title = dict[@"original_title"];
    movie.posterPath = dict[@"poster_path"];
    
    id overview = dict[@"overview"];
    if([overview isKindOfClass:[NSString class]]) {
        movie.overview = overview;
    }
    
    movie.releaseDate = [self.dateFormatter dateFromString:dict[@"release_date"]];
    movie.voteAverage = [dict[@"vote_average"] stringValue];
    movie.isForAdults = [dict[@"adult"] boolValue];
    return movie;
}


#pragma mark - Helper methnds

- (NSURL *)urlForMethod:(NSString *)methodName parameters:(NSDictionary *)params
{
    NSString const *baseURL = @"http://api.themoviedb.org/3/movie/now_playing";
    NSString *result = [baseURL stringByAppendingFormat:@"?api_key=%@", API_KEY];
    for (NSString *key in params) {
        NSString *value = params[key];
        result = [result stringByAppendingFormat:@"&%@=%@", key, value];
    }
    return [NSURL URLWithString:result];
}

@end

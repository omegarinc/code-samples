//
//  MovieViewController.m
//  SwitchMovieGrid
//
//  Created by Alexander Kurbanov on 26.11.15.
//  Copyright © 2015 Alexander Kurbanov. All rights reserved.
//

#import "SDWebImage/UIImageView+WebCache.h"
#import "Movie.h"
#import "MovieViewController.h"

@interface MovieViewController ()

@property (nonatomic, strong) Movie *movie;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewBackground;

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelOverview;

@property (weak, nonatomic) IBOutlet UILabel *labelScore;
@property (weak, nonatomic) IBOutlet UILabel *labelScoreValue;

@property (weak, nonatomic) IBOutlet UILabel *labelRating;
@property (weak, nonatomic) IBOutlet UILabel *labelRatingValue;

@property (weak, nonatomic) IBOutlet UILabel *labelReleaseDate;
@property (weak, nonatomic) IBOutlet UILabel *labelReleaseDateValue;

@end

@implementation MovieViewController

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = self.movie.title;
    
    [self.imageView sd_setImageWithURL:[self.movie urlForPoster]];
    self.imageView.layer.cornerRadius = 5;
    [self.imageViewBackground sd_setImageWithURL:[self.movie urlForPoster]];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:self.movie.releaseDate];
    int year = (int)[components year];
    
    self.labelTitle.text = [NSString stringWithFormat:@"%@ (%i)", self.movie.title, year];
    self.labelOverview.text = self.movie.overview;
    
    self.labelScore.text =  [NSLS(self.labelScore.text) stringByAppendingString:@":"];
    self.labelScoreValue.text = self.movie.voteAverage;
    
    self.labelRating.text = [NSLS(self.labelRating.text) stringByAppendingString:@":"];
    self.labelRatingValue.text = self.movie.isForAdults ? @"R" : @"G";
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd, yyyy"];
    
    self.labelReleaseDate.text = [NSLS(self.labelReleaseDate.text) stringByAppendingString:@":"];
    self.labelReleaseDateValue.text = [formatter stringFromDate:self.movie.releaseDate];
}

#pragma mark - View preparation

- (void)prepareWithMovie:(Movie *)movie
{
    self.movie = movie;
}

@end

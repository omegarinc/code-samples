//
//  MovieManager.h
//  SwitchMovieGrid
//
//  Created by Alexander Kurbanov on 26.11.15.
//  Copyright © 2015 Alexander Kurbanov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ ArrayResultBlock)(NSArray *objects, NSError *error);

@interface MovieManager : NSObject

- (void)loadMoviesAtPage:(int)page completion:(ArrayResultBlock)completion;

@end

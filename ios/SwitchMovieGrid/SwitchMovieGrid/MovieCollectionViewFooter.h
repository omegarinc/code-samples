//
//  MovieCollectionViewFooter.h
//  SwitchMovieGrid
//
//  Created by Alexander Kurbanov on 26.11.15.
//  Copyright © 2015 Alexander Kurbanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MovieCollectionViewFooterDelegate <NSObject>

- (void)didPressButtonLoadMore;

@end

@interface MovieCollectionViewFooter : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UIButton *butonLoadMore;
@property (weak, nonatomic) id<MovieCollectionViewFooterDelegate> delegate;

@end

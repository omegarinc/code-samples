//
//  MovieGridViewController.m
//  SwitchMovieGrid
//
//  Created by Alexander Kurbanov on 25.11.15.
//  Copyright © 2015 Alexander Kurbanov. All rights reserved.
//

#import "SDWebImage/UIImageView+WebCache.h"
#import "MovieCollectionViewCell.h"
#import "MovieCollectionViewFooter.h"
#import "Movie.h"
#import "MovieManager.h"
#import "MovieViewController.h"
#import "MovieGridViewController.h"

#define MOVIE_CELL_REUSE_ID @"movieCollectionViewCell"
#define MOVIE_FOOTER_REUSE_ID @"movieFooterCollectionView"

#define EDGE_OFFSET 16
#define INTERITEM_SPACING 10

@interface MovieGridViewController () <UICollectionViewDataSource, UICollectionViewDelegate, MovieCollectionViewFooterDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong) MovieManager *movieManager;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign) int lastLoadedPage;
@property (nonatomic, strong) NSMutableArray *movies;

@end

@implementation MovieGridViewController

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self prepareNavigationBar];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([MovieCollectionViewCell class]) bundle:nil]
          forCellWithReuseIdentifier:MOVIE_CELL_REUSE_ID];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([MovieCollectionViewFooter class]) bundle:nil]
          forSupplementaryViewOfKind:UICollectionElementKindSectionFooter
                 withReuseIdentifier:MOVIE_FOOTER_REUSE_ID];
    
    self.movies = [NSMutableArray array];
    self.movieManager = [[MovieManager alloc] init];
    [self loadNextMoviePage];
}


#pragma mark - View preparation

- (void)prepareNavigationBar
{
    self.navigationItem.title = NSLS(@"NAVIGATION TITLE Latest movies");
    
    UIButton *button = [[UIButton alloc] init];
    UIImage *image = [UIImage imageNamed:@"icon user gray"];
    button.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    [button setImage:image forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger result = self.movies.count;
    return result;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MovieCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MOVIE_CELL_REUSE_ID forIndexPath:indexPath];
    Movie *movie = self.movies[indexPath.row];
    [cell.imageView sd_setImageWithURL:[movie urlForPoster]];
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    MovieCollectionViewFooter *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter
                                                                           withReuseIdentifier:MOVIE_FOOTER_REUSE_ID
                                                                                  forIndexPath:indexPath];
    footer.delegate = self;
    return footer;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeMake(collectionView.frame.size.width, 100);
}


#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat totalWidth = self.view.frame.size.width - (EDGE_OFFSET * 2) - INTERITEM_SPACING;
    CGFloat itemWidth = totalWidth / 2;
    
    CGFloat itemHeight = itemWidth * 1.4;
    
    return CGSizeMake(itemWidth, itemHeight);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    CGFloat const topOffset = 20;
    return UIEdgeInsetsMake(topOffset, EDGE_OFFSET, 0, EDGE_OFFSET);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 2;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return INTERITEM_SPACING;
}


#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MovieViewController *vc = [[MovieViewController alloc] initWithNibName:NSStringFromClass([MovieViewController class]) bundle:nil];
    Movie *movie = self.movies[indexPath.row];
    [vc prepareWithMovie:movie];
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - MovieCollectionViewFooterDelegate

- (void)didPressButtonLoadMore
{
    [self loadNextMoviePage];
}


#pragma mark - Helper methods

- (void)loadNextMoviePage
{
    if (self.isLoading) {
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    [self updateLoadingState:YES];
    [self.movieManager loadMoviesAtPage:(self.lastLoadedPage + 1) completion:^(NSArray *objects, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            __strong typeof (self) strongSelf = weakSelf;
            if (!strongSelf) {
                return;
            }
            [strongSelf updateLoadingState:NO];
            if (error) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:NSLS(@"COMMON ALERT BUTTON Ok") style:UIAlertActionStyleCancel handler:nil]];
                [strongSelf presentViewController:alert animated:YES completion:nil];
            } else {
                [strongSelf.movies addObjectsFromArray:objects];
                strongSelf.lastLoadedPage++;
                [strongSelf.collectionView reloadData];
            }
        });
    }];
}

- (void)updateLoadingState:(BOOL)isLoading
{
    self.isLoading = isLoading;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = isLoading;
}

@end

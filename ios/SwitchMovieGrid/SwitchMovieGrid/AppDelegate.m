//
//  AppDelegate.m
//  SwitchMovieGrid
//
//  Created by Alexander Kurbanov on 25.11.15.
//  Copyright © 2015 Alexander Kurbanov. All rights reserved.
//

#import "MovieGridViewController.h"
#import "AppDelegate.h"

@implementation AppDelegate

#pragma mark - Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    MovieGridViewController *vc = [[MovieGridViewController alloc] initWithNibName:NSStringFromClass([MovieGridViewController class]) bundle:nil];
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vc];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = nc;
    [self.window makeKeyAndVisible];
    
    [self prepareNavigationBarAppearance];
    return YES;
}


#pragma mark - Helper methods

- (void)prepareNavigationBarAppearance
{
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: RGB_COLOR(150, 150, 170),
                                                           NSFontAttributeName: [UIFont systemFontOfSize:18 weight:UIFontWeightThin]
                                                           }];
}

@end

//
//  MovieCollectionViewCell.h
//  SwitchMovieGrid
//
//  Created by Alexander Kurbanov on 26.11.15.
//  Copyright © 2015 Alexander Kurbanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovieCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

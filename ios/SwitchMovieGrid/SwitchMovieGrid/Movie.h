//
//  Movie.h
//  SwitchMovieGrid
//
//  Created by Alexander Kurbanov on 26.11.15.
//  Copyright © 2015 Alexander Kurbanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Movie : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *posterPath;
@property (nonatomic, strong) NSString *overview;
@property (nonatomic, strong) NSString *voteAverage;
@property (nonatomic, strong) NSDate *releaseDate;

@property (nonatomic, assign) BOOL isForAdults;

- (NSURL *)urlForPoster;

@end

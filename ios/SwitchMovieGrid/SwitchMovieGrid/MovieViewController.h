//
//  MovieViewController.h
//  SwitchMovieGrid
//
//  Created by Alexander Kurbanov on 26.11.15.
//  Copyright © 2015 Alexander Kurbanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Movie;

@interface MovieViewController : UIViewController

- (void)prepareWithMovie:(Movie *)movie;

@end

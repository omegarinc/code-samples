//
//  MovieCollectionViewFooter.m
//  SwitchMovieGrid
//
//  Created by Alexander Kurbanov on 26.11.15.
//  Copyright © 2015 Alexander Kurbanov. All rights reserved.
//

#import "MovieCollectionViewFooter.h"

@implementation MovieCollectionViewFooter

#pragma mark - View lifecycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self.butonLoadMore setTitle:NSLS(self.butonLoadMore.titleLabel.text) forState:UIControlStateNormal];
    self.butonLoadMore.layer.borderColor = RGB_COLOR(182, 184, 195).CGColor;
    self.butonLoadMore.layer.borderWidth = 1;
    self.butonLoadMore.layer.cornerRadius = 4;
}


#pragma mark - Actions

- (IBAction)onButtonLoadMorePressed:(id)sender
{
    [self.delegate didPressButtonLoadMore];
}

@end

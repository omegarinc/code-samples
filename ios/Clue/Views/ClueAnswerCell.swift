//
//  ClueAnswerCell.swift
//
//  Created by Maxim Soloviev on 16/05/16.
//

import UIKit

class ClueAnswerCell: UICollectionViewCell {

    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var labelText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewBackground.layer.cornerRadius = 5
    }
}

//
//  ClueProtocols.swift
//
//  Created by Maxim Soloviev on 29/04/16.
//

import Foundation

// VC (View)

protocol ClueViewProtocol: BaseViewProtocol {
    
    func showClue(clueInfo: ClueQuestionInfo, autoplay: Bool)
    func updateState(state: ClueViewState)
    func enableSkip(enable: Bool)
    func showUserBalance(balance: Int)

    func dismiss()
}

enum ClueViewState {

    case Init
    case Tutorial
    case Clue
    case WaitAnswer
    case CorrectAnswer
    case IncorrectAnswer
}

// Presenter

protocol CluePresenterProtocol: BasePresenterProtocol {
    
    func viewDidLoad()

    func didPressNewClue()
    func didSelectAnswer(index: Int)
    func didPressSkip()
}

// Interactor

protocol ClueInteractorProtocol: BaseInteractorProtocol {
    
    func loadNewClue(completion: ClueInfoResponseWithErrorBlock)
    func userBalance() -> Int
    func didSelectAnswer(index: Int, completion: (isAnswerCorrect: Bool?, error: NSError?) -> Void)
    
    func skipClue(completion: ResponseWithErrorBlock)
}

// Wireframe (Router)

protocol ClueWireframeProtocol: BaseWireframeProtocol {
    
}

typealias ClueInfoResponseWithErrorBlock = (clueInfo: ClueQuestionInfo?, error: NSError?) -> Void

struct ClueQuestionInfo {
    let questionText: String
    let answers: [String]
    let mediaType: String
    let mediaUrl: String
    let thumbnailUrl: String?
    let website: String?
}

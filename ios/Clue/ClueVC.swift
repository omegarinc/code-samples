//
//  ClueVC.swift
//
//  Created by Maxim Soloviev on 16/05/16.
//

import Foundation
import ORCommonUI_Swift
import ORCommonCode_Swift
import AVFoundation
import Player


class GizmoVC: BaseVC {
    var image: UIImage?
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            imageView.image = self.image
        }
    }
}



class ClueVC: BaseVC, ClueViewProtocol, ORInterfaceOrientationRestrictor, PlayerDelegate {
    
    var presenter: CluePresenterProtocol!

    var clueQuestionInfo: ClueQuestionInfo?

    @IBOutlet weak var gradientView: ORGradientView!
    
    @IBOutlet weak var viewClue: UIView!
    
    @IBOutlet weak var viewImageClue: UIView!
    @IBOutlet weak var ivClue: UIImageView!
    @IBOutlet weak var buttonShowQuestion: ORRoundRectButton!
    @IBOutlet weak var viewVideoClue: UIView!
    @IBOutlet weak var viewPlayer: UIView!
    @IBOutlet weak var ivPreview: UIImageView!
    @IBOutlet weak var buttonWebsite: UIButton!
    @IBOutlet weak var buttonClue: UIButton!
    
    @IBOutlet weak var viewTutorial: UIView!
    @IBOutlet weak var buttonCloseTutorial: ORRoundRectButton!
    
    @IBOutlet weak var viewControls: UIView!
    @IBOutlet weak var buttonReplay: UIButton!
    @IBOutlet weak var viewNewClueAndSkip: UIView!
    @IBOutlet weak var buttonNewClue: UIButton!
    @IBOutlet weak var buttonSkip: ORRoundRectButton!
    
    @IBOutlet weak var viewBalance: UIView!
    @IBOutlet weak var labelBalance: UILabel!
    
    @IBOutlet weak var ivCorrectIncorrect: UIImageView!
    @IBOutlet weak var labelCorrectIncorrect: UILabel!
    @IBOutlet weak var labelRewardForClue: UILabel!
    @IBOutlet weak var ivRewardForClue: UIImageView!
    
    @IBOutlet weak var labelQuestion: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var csCollectionViewHeight: NSLayoutConstraint!
    
    var state: ClueViewState = .Init
    var website: String?
    
    var selectedIndex: Int = -1
    let cellLabelColorDefault = UIColor.whiteColor()
    let cellLabelColorNotSelected = UIColor.whiteColor().colorWithAlphaComponent(0.6)
    let cellBackgroundColorDefault = UIColor ( red: 0.3275, green: 0.4087, blue: 0.4247, alpha: 1.0 ).colorWithAlphaComponent(0.7)
    let cellBackgroundColorSelected = UIColor.whiteColor().colorWithAlphaComponent(0.5)
    let cellBackgroundColorNotSelected = UIColor.clearColor()
    
    var isAnswerSelected = false
    
    var player: Player!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        presenter.viewDidLoad()
    }

    func setupUI() {
        buttonWebsite.exclusiveTouch = true
        buttonClue.exclusiveTouch = true
        buttonCloseTutorial.exclusiveTouch = true
        buttonNewClue.exclusiveTouch = true
        buttonShowQuestion.exclusiveTouch = true
        buttonSkip.exclusiveTouch = true
        buttonReplay.exclusiveTouch = true

        DowoodleAppearance.setColor(DowoodleColors.pink, forView: buttonCloseTutorial)
        DowoodleAppearance.setColor(DowoodleColors.pink, forView: buttonNewClue)
        DowoodleAppearance.setColor(DowoodleColors.pink, forView: buttonShowQuestion)
        DowoodleAppearance.setColor(DowoodleColors.gray, forView: buttonWebsite)
        DowoodleAppearance.setColor(DowoodleColors.gray, forView: buttonReplay)
        DowoodleAppearance.setBorder(UIColor.whiteColor(), forView: buttonSkip)
        
        enableSkip(false)

        updateAnswerListHeight()
        
        setupPlayer()
    }
    
    func setupPlayer() {
        player = Player()
        player.delegate = self
        player.view.frame = viewPlayer.bounds
        
        addChildViewController(player)
        viewPlayer.addSubviewWithConstraints(player.view)
        player.didMoveToParentViewController(self)
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return [.LandscapeLeft, .LandscapeRight]
    }
    
    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        return .LandscapeLeft
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    //MARK: - Actions

    @IBAction func cluePressed(sender: AnyObject) {
        showQuestion()
    }
    
    @IBAction func showQuestionPressed(sender: AnyObject) {
        showQuestion()
    }
    
    @IBAction func closeTutorialPressed(sender: AnyObject) {
        updateState(.Clue)
    }
    
    @IBAction func websitePressed(sender: AnyObject) {
        if let w = website, u = NSURL(string: w) {
            UIApplication.sharedApplication().openURL(u)
        }
    }
    
    @IBAction func skipPressed(sender: AnyObject) {
        presenter.didPressSkip()
    }
    
    @IBAction func replayPressed(sender: AnyObject) {
        updateState(.Clue)
    }
    
    @IBAction func newCluePressed(sender: AnyObject) {
        presenter.didPressNewClue()
    }
    
    //MARK: - UICollectionViewDataSource
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let w = collectionView.frame.width / 2
        let size = CGSizeMake(w, (collectionView.collectionViewLayout as!UICollectionViewFlowLayout).itemSize.height)
        return size
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return clueQuestionInfo?.answers.count ?? 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! ClueAnswerCell
        
        let answer = clueQuestionInfo?.answers[indexPath.row]
        cell.labelText.text = answer
        
        var bgColor = cellBackgroundColorDefault
        var textColor = cellLabelColorDefault
        
        switch state {
        case .CorrectAnswer, .IncorrectAnswer:
            bgColor = selectedIndex == indexPath.row ? cellBackgroundColorSelected : cellBackgroundColorNotSelected
            textColor = selectedIndex == indexPath.row ? cellLabelColorDefault : cellLabelColorNotSelected
        default:
            break
        }
        cell.viewBackground.backgroundColor = bgColor
        cell.labelText.textColor = textColor

        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if isAnswerSelected == false {
            isAnswerSelected = true
            selectedIndex = indexPath.row
            collectionView.reloadData()
            presenter.didSelectAnswer(indexPath.row)
        }
    }

    //MARK: - ViewProtocol
    
    func showUserBalance(balance: Int) {
        self.labelBalance.text = NSLS("clue-view-label-balance") + String(balance)
    }
    
    func showClue(clueQuestionInfo: ClueQuestionInfo, autoplay: Bool) {
        isAnswerSelected = false
        selectedIndex = -1
        
        self.clueQuestionInfo = clueQuestionInfo
        website = clueQuestionInfo.website
        
        if clueQuestionInfo.mediaType == kMediaTypeVideo {
            viewImageClue.hidden = true
            viewVideoClue.hidden = false
        } else if clueQuestionInfo.mediaType == kMediaTypeImage {
            viewImageClue.hidden = false
            viewVideoClue.hidden = true
        } else {
            print("Error: invalid clue media type!")
        }

        // show image immediately and show preview for video
        if autoplay || clueQuestionInfo.mediaType == kMediaTypeImage {
            playClue()
        } else {
            if clueQuestionInfo.mediaType == kMediaTypeVideo {
                ivPreview.image = nil
                ORSDWebImageHelper.setImage(imageView: ivPreview, urlString: clueQuestionInfo.thumbnailUrl)
                ivPreview.hidden = false
            }
        }
        
        labelQuestion.text = clueQuestionInfo.questionText.uppercaseString
        
        buttonWebsite.hidden = !(clueQuestionInfo.website != nil && !clueQuestionInfo.website!.isEmpty)
        
        updateAnswerListHeight()
        collectionView.reloadData()
    }

    func updateState(state: ClueViewState) {
        self.state = state
        
        viewClue.hidden = false
        buttonClue.hidden = state != .Clue
        buttonWebsite.hidden = !(state == .Clue && website != nil && !website!.isEmpty)

        viewTutorial.hidden = state != .Tutorial

        viewBalance.hidden = state != .CorrectAnswer
        labelRewardForClue.hidden = state != .CorrectAnswer
        ivRewardForClue.hidden = state != .CorrectAnswer
        
        switch state {
        case .Init:
            viewClue.hidden = true
            viewControls.hidden = true
            
        case .Tutorial:
            viewControls.hidden = true
            fillGradientView(DowoodleColors.gray)
            
        case .Clue:
            viewControls.hidden = true
            buttonShowQuestion.hidden = false
            view.userInteractionEnabled = true
            fillGradientView(UIColor.clearColor())
            playClue()
            
        case .WaitAnswer:
            viewControls.hidden = false
            buttonShowQuestion.hidden = true
            view.userInteractionEnabled = true
            viewNewClueAndSkip.hidden = false
            buttonReplay.hidden = false
            labelCorrectIncorrect.hidden = true
            ivCorrectIncorrect.hidden = true
            fillGradientView(DowoodleColors.gray)

        case .CorrectAnswer:
            isAnswerSelected = false
            UIView.transitionWithView(gradientView, duration: 0.3, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
                self.viewControls.hidden = false
            }, completion: nil)
            buttonShowQuestion.hidden = true
            view.userInteractionEnabled = false
            viewNewClueAndSkip.hidden = true
            buttonReplay.hidden = true
            labelCorrectIncorrect.text = NSLS("clue-correct-answer")
            ivCorrectIncorrect.image = UIImage(named: "icon-correct")
            labelCorrectIncorrect.hidden = false
            ivCorrectIncorrect.hidden = false
            fillGradientView(DowoodleColors.green)
            
        case .IncorrectAnswer:
            isAnswerSelected = false
            UIView.transitionWithView(gradientView, duration: 0.3, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
                self.viewControls.hidden = false
            }, completion: nil)
            buttonShowQuestion.hidden = true
            view.userInteractionEnabled = false
            viewNewClueAndSkip.hidden = true
            buttonReplay.hidden = true
            labelCorrectIncorrect.text = NSLS("clue-incorrect-answer")
            ivCorrectIncorrect.image = UIImage(named: "icon-incorrect")
            labelCorrectIncorrect.hidden = false
            ivCorrectIncorrect.hidden = false
            fillGradientView(DowoodleColors.pink)
        }
        
        collectionView.reloadData()
    }

    func enableSkip(enable: Bool) {
        buttonNewClue.hidden = enable
        buttonSkip.hidden = !enable
    }

    func dismiss() {
        
        //presentingViewController?.dismissViewControllerAnimated(true, completion:nil)
        
        //TEMP
        //upper is the gizmo vc (a dummy vc used for keeping activity list vc context
        //TODO: NEED A PROPER SOLUTION
        if let upper = self.presentingViewController {
            upper.presentingViewController?.dismissViewControllerAnimated(true, completion: {
                
            })
        } else {
            presentingViewController?.dismissViewControllerAnimated(true, completion:nil)
        }
        ///
    }
    
    func showError(message: String?) {
        
        self.isAnswerSelected = false
        
        if message != nil {
            let alertVC = OrientationRestrictedAlertController(title: NSLS("error"), message: message, preferredStyle: UIAlertControllerStyle.Alert)
            alertVC.addAction(UIAlertAction(title: NSLS("ok"), style: UIAlertActionStyle.Cancel, handler: nil))
            
            presentViewController(alertVC, animated: true, completion: nil)
        }
    }
    
    
    //MARK: - PlayerDelegate

    func playerReady(player: Player) {
    }
    
    func playerPlaybackStateDidChange(player: Player) {
        if player.bufferingState == .Ready {
            dispatch_async(dispatch_get_main_queue(), {
                print("hideInView")
                ProgressHUD.hideInView(self.viewClue)
                self.ivPreview.hidden = true
            })
        }
    }
    
    func playerBufferingStateDidChange(player: Player) {
    }
    
    func playerPlaybackWillStartFromBeginning(player: Player) {
    }
    
    func playerPlaybackDidEnd(player: Player) {
        dispatch_async(dispatch_get_main_queue(), {
            self.updateState(.WaitAnswer)
        })
    }

    //MARK: -
    
    func showQuestion() {
        pauseClueIfNeeded()
        updateState(.WaitAnswer)
    }
    
    func fillGradientView(bottomColor: UIColor) {
        gradientView.colorTop = bottomColor.colorWithAlphaComponent(0)
        gradientView.colorBottom = bottomColor
        gradientView.setNeedsDisplay()
    }
    
    func updateAnswerListHeight() {
        csCollectionViewHeight.constant = calcAnswerListHeight(clueQuestionInfo?.answers.count ?? 0)
    }
    
    func calcAnswerListHeight(answerCount: Int) -> CGFloat {
        return (collectionView.collectionViewLayout as! UICollectionViewFlowLayout).itemSize.height * ceil(CGFloat(answerCount) / 2)
    }
    
    func playClue() {
        playClue(mediaType: clueQuestionInfo!.mediaType, mediaUrlStr: clueQuestionInfo!.mediaUrl)
    }
    
    func playClue(mediaType mediaType: String, mediaUrlStr: String) {
        if mediaType == kMediaTypeVideo {
            playVideoClue(mediaUrlStr)
        } else if mediaType == kMediaTypeImage {
            ivPreview.hidden = true
            playImageClue(mediaUrlStr)
        } else {
            print("Error: invalid clue media type!")
        }
    }
    
    func playVideoClue(mediaUrlStr: String) {
        guard let url = NSURL(string: mediaUrlStr) else {
            return
        }

        ProgressHUD.showInView(viewClue, activityIndicatorColor: activityIndicatorColor, bgColor: nil, dimBackground: true)
        print("showInView")

        player.setUrl(url)
        player.fillMode = AVLayerVideoGravityResizeAspectFill
        player.playFromBeginning()
    }
    
    func pauseClueIfNeeded() {
        player.pause()
    }
    
    func playImageClue(mediaUrlStr: String) {
        ORSDWebImageHelper.setImage(imageView: ivClue, urlString: mediaUrlStr)
    }
}

extension UIView {
    func addSubviewWithConstraints(subview: UIView, topOffset: CGFloat = 0, leftOffset: CGFloat = 0, bottomOffset: CGFloat = 0, rightOffset: CGFloat = 0) {
        self.addSubview(subview)
        subview.translatesAutoresizingMaskIntoConstraints = false
        
        self.addConstraint(NSLayoutConstraint.init(item: subview, attribute: .Top, relatedBy: .Equal, toItem: self, attribute: .Top, multiplier: 1, constant: topOffset))
        self.addConstraint(NSLayoutConstraint.init(item: subview, attribute: .Leading, relatedBy: .Equal, toItem: self, attribute: .Leading, multiplier: 1, constant: leftOffset))
        self.addConstraint(NSLayoutConstraint.init(item: subview, attribute: .Bottom, relatedBy: .Equal, toItem: self, attribute: .Bottom, multiplier: 1, constant: -bottomOffset))
        self.addConstraint(NSLayoutConstraint.init(item: subview, attribute: .Trailing, relatedBy: .Equal, toItem: self, attribute: .Trailing, multiplier: 1, constant: -rightOffset))
        
        self.layoutSubviews()
    }
}

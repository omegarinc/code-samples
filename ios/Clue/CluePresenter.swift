//
//  CluePresenter.swift
//
//  Created by Maxim Soloviev on 29/04/16.
//

import Foundation
import ORCommonCode_Swift

class CluePresenter: CluePresenterProtocol {
    
    weak var view: ClueViewProtocol!
    var interactor: ClueInteractorProtocol!
    var wireframe: ClueWireframeProtocol!

    var incorrectAnswerCount = 0
    let incorrectAnswerCountToAllowSkip = 3
    var needToShowTutorial = true
    let showAnswerDuration = NSTimeInterval(2)
    
    func loadNewClue(needToShowTutorial needToShowTutorial: Bool = false) {
        view.showActivityIndicator(true)
        
        view.enableSkip(incorrectAnswerCount >= incorrectAnswerCountToAllowSkip)
        view.showUserBalance(interactor.userBalance())

        interactor.loadNewClue { [weak self] (clueInfo, error) in
            self?.view.showActivityIndicator(false)
            
            if error != nil {
                self?.view.showError(error?.localizedDescription)
                // dismiss after error
                or_dispatch_in_main_queue_after(1, block: {
                    self?.view.dismiss()
                })
            } else {
                self?.view.showClue(clueInfo!, autoplay: !needToShowTutorial)
                self?.view.updateState(needToShowTutorial ? .Tutorial : .Clue)
            }
        }
    }

    //MARK: - BasePresenterProtocol
    
    var baseView: BaseViewProtocol! {
        return view
    }

    //MARK: - PresenterProtocol

    func viewDidLoad() {
        view.updateState(.Init)

        loadNewClue(needToShowTutorial: needToShowTutorial)
        needToShowTutorial = false
    }
    
    func didPressNewClue() {
        loadNewClue()
    }

    func didSelectAnswer(index: Int) {
        
        view.showActivityIndicator(true)
        interactor.didSelectAnswer(index) { [weak self] (isAnswerCorrect, error) in
            
            self?.view.showActivityIndicator(false)
            
            if error != nil {
                self?.view.showError(error!.localizedDescription)
            } else {
                if isAnswerCorrect == true {
                    if let sself = self {
                        sself.view.updateState(.CorrectAnswer)
                        sself.view.showUserBalance(sself.interactor.userBalance())

                        or_dispatch_in_main_queue_after(sself.showAnswerDuration, block: { [weak self] in
                            self?.view.dismiss()
                            or_postNotification(kNotificationClueWasPassed)
                        })
                    }
                } else {
                    if let sself = self {
                        sself.incorrectAnswerCount += 1
                        
                        sself.view.updateState(.IncorrectAnswer)
                        sself.view.showUserBalance(sself.interactor.userBalance())
                        or_dispatch_in_main_queue_after(sself.showAnswerDuration, block: { [weak self] in
                            self?.loadNewClue()
                        })
                    }
                }
            }
        }
    }

    func didPressSkip() {
        interactor.skipClue { [weak self] (error) in
            if error != nil {
                self?.view.showError(error!.localizedDescription)
            } else {
                self?.view.dismiss()
            }
        }
    }
}

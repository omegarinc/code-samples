//
//  ClueWireframe.swift
//
//  Created by Maxim Soloviev on 29/04/16.
//

import Foundation
import UIKit

class ClueWireframe: ClueWireframeProtocol {
    
//    weak var vc: UIViewController!
    
    static func open(fromVC parentVC: UIViewController, dowoodleId: ObjectIdType, activityId: ObjectIdType, experienceEventHandler: ExperienceEventHandlerProtocol) {
        let view = VCRouter.Clue.vc as! ClueVC
        let presenter = CluePresenter()
        let interactor = ClueInteractor(dowoodleId: dowoodleId, activityId: activityId, experienceEventHandler: experienceEventHandler)
        let wireframe = ClueWireframe()
        
        view.presenter = presenter
        presenter.view = view
        presenter.wireframe = wireframe
        presenter.interactor = interactor
        interactor.presenter = presenter

        parentVC.definesPresentationContext = true
        let s = UIStoryboard(name: "Clue", bundle: nil)
        let gizmo = s.instantiateViewControllerWithIdentifier("gizmo") as! GizmoVC
        
        let layer = UIApplication.sharedApplication().keyWindow!.layer
        let scale = UIScreen.mainScreen().scale
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
        
        layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        gizmo.image = image
        
        gizmo.modalPresentationStyle = .Popover
        gizmo.modalTransitionStyle = .CrossDissolve
        
        parentVC.presentViewController(gizmo, animated: true, completion: {
            view.modalPresentationStyle = .OverFullScreen
            view.modalTransitionStyle = .CrossDissolve
            
            gizmo.presentViewController(view, animated: true, completion: nil)
        })
    }
    
    //MARK: - WireframeProtocol

}

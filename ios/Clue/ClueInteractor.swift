//
//  ClueInteractor.swift
//
//  Created by Maxim Soloviev on 29/04/16.
//

import Foundation
import CoreData

class ClueInteractor: BaseExperienceInteractor, ClueInteractorProtocol {
    
    weak var presenter: CluePresenterProtocol!
    
    let activityId: ObjectIdType
    var reward: Int = 0
    var clueQuestion: ClueQuestion?
    
    init(dowoodleId: ObjectIdType, activityId: ObjectIdType, experienceEventHandler: ExperienceEventHandlerProtocol) {
        self.activityId = activityId
        
        super.init(dowoodleId: dowoodleId, experienceEventHandler: experienceEventHandler)
    }

    func getRandomQuestionIndex(questionCount: Int) -> Int{
        return Int(arc4random_uniform(UInt32(questionCount)))
    }
    
    //MARK: - InteractorProtocol
    
    func loadNewClue(completion: ClueInfoResponseWithErrorBlock) {
//        ClueManager.sharedInstance.newClue(dowoodleId: dowoodleId, activityId: activityId) { [weak self] (clue, error) in
        ClueManager.sharedInstance.newRandomClue { [weak self] (clue, error) in
            guard let clue = clue where error == nil else {
                completion(clueInfo: nil, error: error)
                return
            }

            if let sself = self {
                let questionIndex = sself.getRandomQuestionIndex(clue.questions.count)
                let q = clue.questions[questionIndex]
                sself.clueQuestion = q
                sself.reward = clue.reward
                
                let clueQuestionInfo = ClueQuestionInfo(questionText: q.questionText, answers: q.answers, mediaType: clue.mediaType,
                    mediaUrl: clue.mediaUrl, thumbnailUrl: clue.thumbnailUrl, website: clue.website)
                completion(clueInfo: clueQuestionInfo, error: error)
            }
        }
    }
    
    func userBalance() -> Int {
        return UserManager.sharedInstance.currentUserDartsBalance()
    }
    
    func didSelectAnswer(index: Int, completion: (isAnswerCorrect: Bool?, error: NSError?) -> Void) {
        if index == clueQuestion!.rightAnswerIndex {
            experienceEventHandler.openActivity(dowoodleId: dowoodleId, activityId: activityId, completion: { (event, error) in
                if error != nil {
                   completion(isAnswerCorrect: false, error: error)
                } else {
                    
                    UserManager.sharedInstance.giveDartsReward(self.reward, completion: {_,_ in
                        completion(isAnswerCorrect: true, error: nil)
                    })
                }
            })
        } else {
            completion(isAnswerCorrect: false, error: nil)
        }
    }
    
    func skipClue(completion: ResponseWithErrorBlock) {
        experienceEventHandler.openActivity(dowoodleId: dowoodleId, activityId: activityId, completion: { (event, error) in
            completion(error: error)
        })
    }
}

//
//  DynamicCollectionViewLayout.swift
//
//  Created by Maxim Soloviev on 15/03/16.
//

import UIKit

class RectangleAttachmentBehavior : UIDynamicBehavior {
 
    var anchorPoint = CGPointZero
    var item: UIDynamicItem
    var size: CGFloat
    
    init(withItem item: UIDynamicItem, point: CGPoint, damping: CGFloat, frequency: CGFloat, length: CGFloat, size: CGFloat) {
        self.item = item
        self.size = size
        
        super.init()

        anchorPoint = point
        
        let anchorPoints = calcAnchorPoints(centerPoint: point)
    
        for anchorPoint in anchorPoints {
            let attachmentBehavior = UIAttachmentBehavior(item: item, attachedToAnchor: anchorPoint)
            attachmentBehavior.frequency = frequency
            attachmentBehavior.damping = damping
            addChildBehavior(attachmentBehavior)
        }
    }
    
    func updateAttachmentLocation(newPoint: CGPoint) {
        anchorPoint = newPoint

        let anchorPoints = calcAnchorPoints(centerPoint: newPoint)
        
        for i in 0 ..< anchorPoints.count {
            let attachmentBehavior = childBehaviors[i] as! UIAttachmentBehavior
            attachmentBehavior.anchorPoint = anchorPoints[i]
        }
    }
    
    func updateAttachmentParams(damping damping: CGFloat, frequency: CGFloat, length: CGFloat) {
        for childBehavior in childBehaviors {
            if let attachmentBehavior = childBehavior as? UIAttachmentBehavior {
                attachmentBehavior.damping = damping
                attachmentBehavior.frequency = frequency
            }
        }
    }
    
    func calcAnchorPoints(centerPoint centerPoint: CGPoint) -> [CGPoint] {
        let points = [CGPointMake(centerPoint.x - size / 2.0, centerPoint.y - size / 2.0), CGPointMake(centerPoint.x + size / 2.0, centerPoint.y - size / 2.0),
            CGPointMake(centerPoint.x + size / 2.0, centerPoint.y + size / 2.0), CGPointMake(centerPoint.x - size / 2.0, centerPoint.y + size / 2.0)]

        return points
    }
}

class DynamicCollectionViewLayout: BaseCollectionViewLayout {

    var scaleSelection = false
    let selectedCellScale = CGFloat(1.3)
    var selectionChangedTime: NSTimeInterval = 0
    let selectionChangeAnimDuration = 0.5
    
    var collisionsEnabled = false {
        didSet {
            dynamicAnimator.removeAllBehaviors()
            invalidateLayout()
        }
    }

    var size = CGFloat(100)
    var frequency = CGFloat(0.4) {
        didSet {
            updateAttachmentParams()
        }
    }
    var damping = CGFloat(0.2) {
        didSet {
            updateAttachmentParams()
        }
    }
    var length = CGFloat(25) {
        didSet {
            updateAttachmentParams()
        }
    }
    var resistance = CGFloat(250)
    
    lazy var dynamicAnimator: UIDynamicAnimator = UIDynamicAnimator(collectionViewLayout: self)
    
    func updateAttachmentParams() {
        for behavior in dynamicAnimator.behaviors {
            if let attachmentBehavior = behavior as? RectangleAttachmentBehavior {
                attachmentBehavior.updateAttachmentParams(damping: damping, frequency: frequency, length: length)
            }
        }
    }

    override func collectionViewContentSize() -> CGSize {
        if pagingEnabled {
            return CGSizeMake(CGFloat(sectionsCount + 5) * cellSize, CGFloat(sectionsCount + 5) * cellSize)
        } else {
            let diameter = cellSize + CGFloat(sectionsCount * 2 - 1) * interCellDistance
            let frameSize = UIScreen.mainScreen().bounds
            return CGSizeMake(max(diameter, frameSize.width), max(diameter, frameSize.height))
        }
    }
    
    override func shouldInvalidateLayoutForBoundsChange(newBounds: CGRect) -> Bool {
        let deltaX: CGFloat = newBounds.origin.x - collectionView!.bounds.origin.x
        let deltaY: CGFloat = newBounds.origin.y - collectionView!.bounds.origin.y
        
        let touchLocation: CGPoint = collectionView!.panGestureRecognizer.locationInView(collectionView)

        var resistance = self.resistance
        let selectionChangeAnimTime = self.selectionChangeAnimTime
        if selectionChangeAnimTime < selectionChangeAnimDuration {
            resistance = 1200
        }
        
        for behavior in dynamicAnimator.behaviors {
            if let attachmentBehavior = behavior as? RectangleAttachmentBehavior {
                let distance = ORMath.distance(touchLocation, attachmentBehavior.anchorPoint)
                let scrollResistance = distance / resistance
                
                let item = attachmentBehavior.item
                var center = item.center
                
                if deltaX < 0 {
                    center.x += max(deltaX, deltaX * CGFloat(scrollResistance))
                } else {
                    center.x += min(deltaX, deltaX * CGFloat(scrollResistance))
                }

                if deltaY < 0 {
                    center.y += max(deltaY, deltaY * CGFloat(scrollResistance))
                } else {
                    center.y += min(deltaY, deltaY * CGFloat(scrollResistance))
                }

                item.center = center
                
                dynamicAnimator.updateItemUsingCurrentState(item)
            }
        }
        
        return false
    }
    
    override func layoutAttributesForElementsInRect(rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        if let attributesArray = dynamicAnimator.itemsInRect(rect) as? [UICollectionViewLayoutAttributes] {
            for attributes in attributesArray {
                let scale = scalingEnabled ? scaleForItemAtIndexPath(attributes.indexPath) : 1
                attributes.transform = CGAffineTransformMakeScale(scale, scale)
            }
            return attributesArray
        }
        return nil
    }
    
    override func layoutAttributesForItemAtIndexPath(indexPath: NSIndexPath) -> UICollectionViewLayoutAttributes? {
        return dynamicAnimator.layoutAttributesForCellAtIndexPath(indexPath)
    }
    
    override func prepareLayout() {
        super.prepareLayout()
        
        let items = layoutAttributesForAllElements()
        
        if (dynamicAnimator.behaviors.count == 0) {
            for item in items {
                let attachmentBehavior = RectangleAttachmentBehavior(withItem: item, point: item.center,
                    damping: damping, frequency: frequency, length: length, size: cellSize)
                dynamicAnimator.addBehavior(attachmentBehavior)
            }
            
            if collisionsEnabled {
                let collisionBehavior = UICollisionBehavior(items: items)
                dynamicAnimator.addBehavior(collisionBehavior)
            }
        }
    }
    
    func layoutAttributesForAllElements() -> [UICollectionViewLayoutAttributes] {
        var attributes = [UICollectionViewLayoutAttributes]()
        
        for section in 0 ..< sectionsCount {
            let cellCount = collectionView!.numberOfItemsInSection(section)
            
            for i in 0 ..< cellCount {
                let indexPath = NSIndexPath(forItem: i, inSection: section)
                attributes.append(defaultLayoutAttributesForItemAtIndexPath(indexPath)!)
            }
        }
        
        return attributes
    }

    func defaultLayoutAttributesForItemAtIndexPath(indexPath: NSIndexPath) -> UICollectionViewLayoutAttributes? {
        let attributes = UICollectionViewLayoutAttributes(forCellWithIndexPath: indexPath)
        
        attributes.size = CGSizeMake(cellSize, cellSize)
        attributes.center = cellPositionForIndexPath(indexPath, interCellDistance: interCellDistance)
        let scale = scalingEnabled ? scaleForItemAtIndexPath(indexPath) : 1
        attributes.transform = CGAffineTransformMakeScale(scale, scale)
        
        return attributes
    }
    
    func scaleForItemAtIndexPath(indexPath: NSIndexPath) -> CGFloat {
        var scale: CGFloat?
        
        if scaleSelection
            && (indexPath == currentSelectedIndexPath || indexPath == prevSelectedIndexPath) {
            if selectionChangeAnimTime < selectionChangeAnimDuration {
                let scaleCoef = CGFloat(selectionChangeAnimTime / selectionChangeAnimDuration)
                let defaultScale = defaultScaleForItemAtIndexPath(indexPath)
                
                if currentSelectedIndexPath == indexPath {
                    scale = defaultScale + (selectedCellScale - defaultScale) * scaleCoef
                } else {
                    scale = defaultScale + (selectedCellScale - defaultScale) * (1 - scaleCoef)
                }
            } else if indexPath == currentSelectedIndexPath {
                scale = selectedCellScale
            }
        }

        if scale == nil {
            scale = defaultScaleForItemAtIndexPath(indexPath)
        }
        scale = scale! * additionalScaleCoef * cellSizeScaleForCurrentDevice
        
        return scale!
    }
    
    func defaultScaleForItemAtIndexPath(indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 1.2
        case 1:
            return 1.0
        case 2:
            return 0.8
        default:
            return 0.6
        }
    }
    
    var selectionChangeAnimTime: NSTimeInterval {
        return CACurrentMediaTime() - selectionChangedTime
    }

    override func selectCell(indexPath: NSIndexPath) {
        selectionChangedTime = CACurrentMediaTime()
        super.selectCell(indexPath)
    }
    
    override func reset() {
        super.reset()
        dynamicAnimator.removeAllBehaviors()
    }
}

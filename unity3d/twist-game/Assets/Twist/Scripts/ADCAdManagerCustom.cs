﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Twist
{
    public class ADCAdManagerCustom : MonoBehaviour
    {

#if ADCOLONY

	private static ADCAdManagerCustom _instance;

	public static ADCAdManagerCustom Instance
	{
		get {
			if(_instance == null)
			{
				_instance = FindObjectOfType( typeof(ADCAdManagerCustom) ) as ADCAdManagerCustom;
				if(_instance == null)
				{
					_instance = (new GameObject("ADCAdManagerCustom")).AddComponent<ADCAdManagerCustom>();
				}
			}
			return _instance;
		}
	}

	public int onVideoFinishedCounter = 0;
	public int onVideoFinishedWithInfoCounter = 0;
	public int onV4VCResultCounter = 0;
	public int onAdAvailabilityChangeCounter = 0;

	public int GetCounter(string counterName) 
    {
		switch(counterName)
        {
		case "VideoFinished":
			return onVideoFinishedCounter;
		case "VideoFinishedWithInfo":
			return onVideoFinishedWithInfoCounter;
		case "V4VC":
			return onV4VCResultCounter;
		case "AdAvailable":
			return onAdAvailabilityChangeCounter;
		default:
			return -1;
		}
	}
        
	public int regularCurrency = 0;
	public string version = "1.1";
	public string appId = "";
	public Dictionary<string, ADCVideoZoneCustom> videoZones = new Dictionary<string, ADCVideoZoneCustom>();

	private void Awake()
    {
		ConfigureADCPlugin();

		AddOnVideoStartedMethod(OnVideoStarted);
		AddOnVideoFinishedMethod(OnVideoFinished);
		AddOnVideoFinishedWithInfoMethod(OnVideoFinishedWithInfo);
		AddOnV4VCResultMethod(OnV4VCResult);
		AddOnAdAvailabilityChangeMethod(OnAdAvailabilityChange);

		DontDestroyOnLoad(this.gameObject);
	}


	public void Pause() 
	{
		SoundManager a = FindObjectOfType<SoundManager>();

		if (a != null) 
		{
			a.MuteAllMusic();
		}

		Time.timeScale = 0;
	}

	public void Resume()
	{
		SoundManager a = FindObjectOfType<SoundManager>();

		if (a != null) 
		{
			a.UnmuteAllMusic();
		}

		Time.timeScale = 1;
		GC.Collect();
		Application.targetFrameRate = 60;
	}

	public void ConfigureADCPlugin()
    {
		ConfigureZones();

		AdColony.Configure (version, // Arbitrary app version
			appId,   // ADC App ID from adcolony.com
			GetVideoZoneIdsAsStringArray());
	}

	public void ConfigureZones()
    {
		appId = AdsManager.instance.ADCOLONY_appId;
		AddZoneToManager(AdsManager.instance.ADCOLONY_InterstitialVideoZone1KEY, AdsManager.instance.ADCOLONY_InterstitialVideoZone1ID, ADCVideoZoneTypeCustom.Interstitial);
		AddZoneToManager(AdsManager.instance.ADCOLONY_InterstitialVideoZone2KEY, AdsManager.instance.ADCOLONY_InterstitialVideoZone2ID, ADCVideoZoneTypeCustom.Interstitial);
		AddZoneToManager(AdsManager.instance.ADCOLONY_RewardedVideoZoneKEY, AdsManager.instance.ADCOLONY_RewardedVideoZoneID, ADCVideoZoneTypeCustom.V4VC);
	}

	private void OnVideoStarted()
    {
		Pause();
	}

	private void OnVideoFinished(bool adWasShown)
    {
		++onVideoFinishedCounter;

		Debug.Log("On Video Finished Counter " + onVideoFinishedCounter);
		Debug.Log("On Video Finished, and Ad was shown: " + adWasShown);

		Resume();
	}

	private void OnVideoFinishedWithInfo(AdColonyAd ad)
    {
		++onVideoFinishedWithInfoCounter;

		Debug.Log("On Video Finished With Info, ad Played: " + ad.toString());

		if (ad.iapEnabled)
        {
			AdColony.NotifyIAPComplete("ProductID", "TransactionID", null, 0, 1);
		}

		Resume();
	}

	private void OnV4VCResult(bool success, string name, int amount)
    {
		++onV4VCResultCounter;

		if (success)
        {
			Debug.Log("V4VC SUCCESS: name = " + name + ", amount = " + amount);

			AddToCurrency(amount);
		}
        else
        {
			Debug.LogWarning("V4VC FAILED!");
		}
	}

	private void OnAdAvailabilityChange( bool avail, string zoneId)
    {
		++onAdAvailabilityChangeCounter;

		Debug.Log("Ad Availability Changed to available=" + avail + " In zone: "+ zoneId);
	}

	public static void AddOnVideoStartedMethod(AdColony.VideoStartedDelegate onVideoStarted)
    {
		AdColony.OnVideoStarted += onVideoStarted;
	}

	public static void RemoveOnVideoStartedMethod(AdColony.VideoStartedDelegate onVideoStarted) 
    {
		AdColony.OnVideoStarted -= onVideoStarted;
	}

	public static void AddOnVideoFinishedMethod(AdColony.VideoFinishedDelegate onVideoFinished)
    {
		AdColony.OnVideoFinished += onVideoFinished;
	}

	public static void RemoveOnVideoFinishedMethod(AdColony.VideoFinishedDelegate onVideoFinished)
    {
		AdColony.OnVideoFinished -= onVideoFinished;
	}

	public static void AddOnVideoFinishedWithInfoMethod(AdColony.VideoFinishedWithInfoDelegate onVideoFinishedWithInfo)
    {
		AdColony.OnVideoFinishedWithInfo += onVideoFinishedWithInfo;
	}

	public static void RemoveOnVideoFinishedWithInfoMethod(AdColony.VideoFinishedWithInfoDelegate onVideoFinishedWithInfo)
    {
		AdColony.OnVideoFinishedWithInfo -= onVideoFinishedWithInfo;
	}

	public static void AddOnV4VCResultMethod(AdColony.V4VCResultDelegate onV4VCResult)
    {
		AdColony.OnV4VCResult += onV4VCResult;
	}

	public static void RemoveOnV4VCResultMethod(AdColony.V4VCResultDelegate onV4VCResult)
    {
		AdColony.OnV4VCResult -= onV4VCResult;
	}

	public static void AddOnAdAvailabilityChangeMethod(AdColony.AdAvailabilityChangeDelegate onAdAvailabilityChange)
    {
		AdColony.OnAdAvailabilityChange += onAdAvailabilityChange;
	}

	public static void RemoveOnAdAvailabilityChangeMethod(AdColony.AdAvailabilityChangeDelegate onAdAvailabilityChange)
    {
		AdColony.OnAdAvailabilityChange -= onAdAvailabilityChange;
	}

	public static void AddToCurrency(int amountToAddToCurrency)
    {
		ADCAdManagerCustom.Instance.regularCurrency += amountToAddToCurrency;
	}

	public static int GetRegularCurrencyAmount()
    {
		return ADCAdManagerCustom.Instance.regularCurrency;
	}

	public static void ResetADCAdManagerCustomZones()
    {
		ADCAdManagerCustom.GetVideoZonesDictionary().Clear();
	}

	public static void AddZoneToManager(string zoneKey, string zoneId, ADCVideoZoneTypeCustom videoZoneType)
    {
		zoneKey = zoneKey.ToLower();

		if (ContainsZoneKey(zoneKey))
        {
			Debug.LogWarning("The ad manager overwrote the previous video zoneId: " + GetZoneIdByKey(zoneKey) + " for the video zone named " + zoneKey + " with the new video zoneId of: " + zoneId);
		}
		else
        {
			Debug.LogWarning("The ad manager has added the video zone named " + zoneKey + " with the video zoneId of: " + zoneId);

			ADCAdManagerCustom.GetVideoZonesDictionary().Add(zoneKey, new ADCVideoZoneCustom(zoneId, videoZoneType));
		}
	}

	public static ADCVideoZoneCustom GetVideoZoneObjectByKey(string key)
    {
		key = key.ToLower();

		if (ContainsZoneKey(key))
        {
			return ADCAdManagerCustom.GetVideoZonesDictionary()[key];
		}
		else
        {
			return null;
		}
	}

	public static string GetZoneIdByKey(string key)
    {
		key = key.ToLower();

		if (ContainsZoneKey(key))
        {
			return ADCAdManagerCustom.GetVideoZonesDictionary()[key].zoneId;
		}
		else
        {
			return "";
		}
	}

	public static bool ContainsZoneKey(string key)
    {
		key = key.ToLower();

		if (GetVideoZonesDictionary().ContainsKey(key))
        {
			return true;
		}
		else
        {
			return false;
		}
	}

	public static void RemoveZoneFromManager(string zoneKey)
    {
		zoneKey = zoneKey.ToLower();
		ADCAdManagerCustom.GetVideoZonesDictionary().Remove(zoneKey);
	}

	public static string[] GetVideoZoneIdsAsStringArray()
    {
		Dictionary<string, ADCVideoZoneCustom> videoZones = GetVideoZonesDictionary();
		string[] allZones = new string[GetVideoZonesDictionary().Count];
		int currentKeyValuePair = 0;

		foreach(KeyValuePair<string, ADCVideoZoneCustom> keyValuePair in videoZones)
        {
			allZones[currentKeyValuePair] = keyValuePair.Value.zoneId;
			currentKeyValuePair++;
		}

		return allZones;
	}

	public static Dictionary<string, ADCVideoZoneCustom> GetVideoZonesDictionary()
    {
		return ADCAdManagerCustom.Instance.videoZones;
	}

	public static void ShowVideoAdByZoneKey(string zoneIdKey, bool offerV4VCBeforePlay = false, bool showPopUpAfter = false)
    {
		ADCVideoZoneCustom videoZone = GetVideoZoneObjectByKey(zoneIdKey);
		string zoneId = GetZoneIdByKey(zoneIdKey);

		if (videoZone.zoneType == ADCVideoZoneTypeCustom.Interstitial && AdColony.IsVideoAvailable(zoneId))
        {
			AdColony.ShowVideoAd(zoneId);
		}
		else if (videoZone.zoneType == ADCVideoZoneTypeCustom.V4VC && AdColony.IsV4VCAvailable(zoneId))
        {
			if (offerV4VCBeforePlay)
            {
				AdColony.OfferV4VC(showPopUpAfter, zoneId);
			}
            else
            {
				AdColony.ShowV4VC(showPopUpAfter, zoneId);
			}
		}
        else
        {
			Debug.Log("AdColony ---- The zone '" + zoneId + "' was requested to play, but it is NOT ready to play yet.");
		}
	}

#endif

    }
}
﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

namespace Twist
{
    public class RateUsManager : MonoBehaviour
    {
        #region Public resources
        public int NumberOfLevelPlayedToShowRateUs = 30;
        public string iOSURL = "itms://itunes.apple.com/us/app/apple-store/idxxxxxxxx?mt=8";
        public string ANDROIDURL = "http://app-advisory.com";

        public Button btnYes;
        public Button btnLater;
        public Button btnNever;

        public CanvasGroup popupCanvasGroup;
        #endregion

        #region System methods
        private void Awake()
        {
            popupCanvasGroup.alpha = 0;
            popupCanvasGroup.gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            GameManager.OnGameEnded += CheckIfPromptRateDialogue;
        }

        private void OnDisable()
        {
            GameManager.OnGameEnded -= CheckIfPromptRateDialogue;
        }
        #endregion

        #region Public methods
        public void PromptPopup()
        {
            popupCanvasGroup.alpha = 0;
            popupCanvasGroup.gameObject.SetActive(true);

            StartCoroutine(DoLerpAlpha(popupCanvasGroup, 0, 1, 1, () =>
            {
                AddButtonListeners();
            }));
        }

        public IEnumerator DoLerpAlpha(CanvasGroup c, float from, float to, float time, Action callback)
        {
            float timer = 0;
            c.alpha = from;

            while (timer <= time)
            {
                timer += Time.deltaTime;
                c.alpha = Mathf.Lerp(from, to, timer / time);

                yield return null;
            }

            c.alpha = to;

            if (callback != null)
            {
                callback();
            }
        }
        #endregion

        #region Private methods
        private void AddButtonListeners()
        {
            btnYes.onClick.AddListener(OnClickedYes);
            btnLater.onClick.AddListener(OnClickedLater);
            btnNever.onClick.AddListener(OnClickedNever);
        }

        private void RemoveButtonListener()
        {
            btnYes.onClick.RemoveListener(OnClickedYes);
            btnLater.onClick.RemoveListener(OnClickedLater);
            btnNever.onClick.RemoveListener(OnClickedNever);
        }

        private void OnClickedYes()
        {

#if UNITY_IPHONE

		Application.OpenURL(iOSURL);

#endif

#if UNITY_ANDROID

		Application.OpenURL(ANDROIDURL);

#endif

            PlayerPrefs.SetInt("NUMOFLEVELPLAYED", -1);
            PlayerPrefs.Save();
            HidePopup();
        }

        private void OnClickedLater()
        {
            PlayerPrefs.SetInt("NUMOFLEVELPLAYED", 0);
            PlayerPrefs.Save();
            HidePopup();
        }

        private void OnClickedNever()
        {
            PlayerPrefs.SetInt("NUMOFLEVELPLAYED", -1);
            PlayerPrefs.Save();
            HidePopup();
        }

        private void CheckIfPromptRateDialogue()
        {
            int count = PlayerPrefs.GetInt("NUMOFLEVELPLAYED", 0);

            if (count == -1)
            {
                return;
            }

            count++;

            if (count > NumberOfLevelPlayedToShowRateUs)
            {
                PromptPopup();
            }
            else
            {
                PlayerPrefs.SetInt("NUMOFLEVELPLAYED", count);
            }

            PlayerPrefs.Save();
        }

        private void HidePopup()
        {
            StartCoroutine(DoLerpAlpha(popupCanvasGroup, 1, 0, 1, () =>
            {
                popupCanvasGroup.gameObject.SetActive(false);
                RemoveButtonListener();
            }));
        }
        #endregion
    }
}


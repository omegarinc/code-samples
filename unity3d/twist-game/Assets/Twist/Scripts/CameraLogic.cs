﻿using UnityEngine;
using System.Collections;

namespace Twist
{
    public class CameraLogic : MonoBehaviour 
    {
        #region Public resources
        public Transform player;
        #endregion

        #region System methods
        private void Start()
	    {
		    var p = FindObjectOfType<Player> ();
		    player = p.transform;
	    }

	    private void Update()
	    {
		    transform.rotation = player.rotation;	
	    }
        #endregion
    }
}

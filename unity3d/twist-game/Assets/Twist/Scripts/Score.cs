﻿using UnityEngine;
using System.Collections;

namespace Twist
{
    public static class Score
    {
        #region Public methods
        public static bool SaveLastScore(int score)
        {
            PlayerPrefs.SetInt("LAST_SCORE", score);

            int best = LoadBestScore();

            if (score > best)
            {
                PlayerPrefs.SetInt("BEST_SCORE", score);
                PlayerPrefsX.SetBool("LastScoreWasBestScore", true);
            }
            else
            {
                PlayerPrefsX.SetBool("LastScoreWasBestScore", false);
            }

            PlayerPrefs.Save();

            return LastScoreWasBestScore();
        }

        public static int LoadLastScore()
        {
            return PlayerPrefs.GetInt("LAST_SCORE", 0);
        }

        public static bool LastScoreWasBestScore()
        {
            return PlayerPrefsX.GetBool("LastScoreWasBestScore", false);
        }

        public static int LoadBestScore()
        {
            return PlayerPrefs.GetInt("BEST_SCORE", 0);
        }
        #endregion
    }
}

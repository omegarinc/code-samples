﻿using UnityEngine;
using System.Collections;

namespace Twist
{
    public class SoundManager : MonoBehaviour
    {
        #region Public resources
        public AudioClip soundJump;
        public AudioClip soundFail;
        #endregion

        #region Private resources
        private AudioSource audioSource;
        #endregion

        #region System methods
        private void Awake()
        {
            audioSource = GetComponent<AudioSource>();
        }
        #endregion

        #region Public methods
        public void PlaySoundFail()
        {
            audioSource.PlayOneShot(soundFail);
        }

        public void PlaySoundJump()
        {
            audioSource.PlayOneShot(soundJump);
        }
        #endregion
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Twist
{
    public class GameManager : MonoBehaviorHelper
    {
        #region Delegates & Events
        public delegate void GameStart();
        public static event GameStart OnGameStarted;

        public delegate void GameEnd();
        public static event GameEnd OnGameEnded;
        #endregion

        #region Serialized fields
        [SerializeField]
        private GameObject AdsManagerPrefab;
        [SerializeField]
        private Color[] colors;
        [SerializeField]
        private Material platformMaterial;
        [SerializeField]
        private Color currentPlatformColor;
        [SerializeField]
        private GameObject platformPrefab;
        #endregion

        #region Private resources
        private List<GameObject> listPlatform = new List<GameObject>();
        private int positionCube = 0;
        private int spawnCount = 1;
        private System.Random rand = new System.Random();
        #endregion

        #region Public methods
        public int GetPlayerLife()
        {
            int l = PlayerPrefs.GetInt("LIFE", 3);

            return l;
        }

        public void SetPlayerLife(int l)
        {
            PlayerPrefs.SetInt("LIFE", l);
            PlayerPrefs.Save();
        }

        public void UseOneLife()
        {
            int l = GetPlayerLife();
            l--;
            SetPlayerLife(l);
        }

        public void OnGameStart()
        {
            if (OnGameStarted != null)
            {
                OnGameStarted();
            }
        }

        public void OnGameEnd()
        {
            if (OnGameEnded != null)
            {
                OnGameEnded();
            }
        }

        public Transform SpawnPlatform(int pos)
        {
            Transform t = GetPooledPlatform();
            t.SetParent(transform, true);
            t.position = new Vector3(0, 0, 2 * spawnCount);
            positionCube = pos;
            spawnCount++;
            t.name = spawnCount.ToString();
            t.GetComponent<AnimationCube>().DoPosition();

            return t;
        }
        #endregion

        #region System methods
        private void Awake()
        {
            int count = 0;

            while (true)
            {
                GameObject o = Instantiate(platformPrefab) as GameObject;

                Transform t = o.transform;
                t.SetParent(transform, true);
                o.SetActive(false);
                listPlatform.Add(o);

                count++;

                if (count > 30)
                {
                    break;
                }
            }

            var adsManager = FindObjectOfType<AdsManager>();

            if (adsManager == null)
            {
                var o = Instantiate(AdsManagerPrefab) as GameObject;
                o.SetActive(true);
            }
        }

        private IEnumerator Start()
        {
            platformMaterial.color = colors[0];

            Application.targetFrameRate = 60;
            GC.Collect();

            yield return 0;

            StartCoroutine(CoroutSpawnCube());
            StartCoroutine(ChangeMaterialColor());
        }

        private void OnDisable()
        {
            platformMaterial.color = colors[0];
        }

        private void OnApplicationQuit()
        {
            platformMaterial.color = colors[0];
        }
        #endregion


        #region Private methods
        private Transform GetPooledPlatform()
        {
            var p = listPlatform.Find(o => o.activeInHierarchy == false);

            if (p == null)
            {
                GameObject o = Instantiate(platformPrefab) as GameObject;
                Transform t = o.transform;
                t.SetParent(transform, true);
                o.SetActive(false);
                listPlatform.Add(o);
                p = o;
            }

            p.SetActive(true);

            return p.transform;
        }

        private IEnumerator CoroutSpawnCube()
        {
            while (true)
            {
                if (spawnCount < 5)
                {
                    SpawnPlatform(0);
                }
                else
                {
                    yield return new WaitForSeconds(0.2f);

                    if (GetRandom() == 0)
                    {
                        positionCube--;
                    }
                    else
                    { 
                        positionCube++;
                    }

                    var t = SpawnPlatform(positionCube);
                    t.eulerAngles = new Vector3(0, 0, positionCube * 45);
                }

                while (listPlatform.FindAll(o => o.activeInHierarchy == true).Count > 20)
                {
                    yield return 0;
                }
            }
        }

        private int GetRandom()
        {
            return rand.Next(0, 2);
        }

        private IEnumerator ChangeMaterialColor()
        {
            yield return new WaitForSeconds(5f);

            while (true)
            {
                Color colorTemp = colors[UnityEngine.Random.Range(0, colors.Length)];
                StartCoroutine(DoLerp(platformMaterial.color, colorTemp, 3f));

                yield return new WaitForSeconds(10f);
            }
        }
       
        private IEnumerator DoLerp(Color from, Color to, float time)
        {
            float timer = 0;

            while (timer <= time)
            {
                timer += Time.deltaTime;
                platformMaterial.color = Color.Lerp(from, to, timer / time);

                yield return null;
            }

            platformMaterial.color = to;
        }
        #endregion
    }
}

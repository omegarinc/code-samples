﻿using UnityEngine;
using System.Collections;

namespace Twist
{
    public class MoveForward : MonoBehaviorHelper
    {
        #region Public resources
        public float sensitivity = 1f;
        #endregion

        #region Private resources
        private bool isStarted = false;
        #endregion

        #region System methods
        private void OnEnable()
        {
            GameManager.OnGameStarted += OnGameStarted;
            GameManager.OnGameEnded += OnGameEnded;
        }

        private void OnDisable()
        {
            GameManager.OnGameStarted -= OnGameStarted;
            GameManager.OnGameEnded -= OnGameEnded;
        }

        private void Update()
        {
            if (!isStarted)
            {
                return;
            }

            transform.Translate(Vector3.forward * sensitivity * Time.deltaTime);
        }
        #endregion

        #region Private metods
        private void OnGameStarted()
        {
            isStarted = true;
        }

        private void OnGameEnded()
        {
            isStarted = false;
        }
        #endregion
    }
}
﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

namespace Twist
{
    public class InputTouch : MonoBehaviour
    {
        #region Delegates & Events
        public delegate void TouchLeft();
        public static event TouchLeft OnTouchLeft;

        public delegate void TouchRight();
        public static event TouchRight OnTouchRight;

        public delegate void TouchScreen();
        public static event TouchScreen OnTouchScreen;
        #endregion

#if UNITY_TVOS

	    private Vector2 startPosition;
        private bool gameStarted = true;

	    private void Awake()
	    {
		    OnGameStart();
	    }

        private void Start()
	    {
		    UnityEngine.Apple.TV.Remote.reportAbsoluteDpadValues = true;
	    }

	    private void OnGameStart()
	    {
		    UnityEngine.Apple.TV.Remote.touchesEnabled = true;
		    UnityEngine.Apple.TV.Remote.allowExitToHome = false;

		    gameStarted = true;

		    FindObjectOfType<StandaloneInputModule>().forceModuleActive = false;
	    }

	    public void OnGameOver()
	    {
		    print("do game over");
		    UnityEngine.Apple.TV.Remote.touchesEnabled = false;
		    UnityEngine.Apple.TV.Remote.allowExitToHome = true;

		    FindObjectOfType<StandaloneInputModule>().forceModuleActive = true;

		    var es = FindObjectOfType<EventSystem>();

		    es.firstSelectedGameObject = es.currentSelectedGameObject;
		    es.SetSelectedGameObject(es.currentSelectedGameObject);

		    gameStarted = false;
	    }

#endif

        #region System methods
        private void Update()
        {

#if UNITY_TVOS && !UNITY_EDITOR
        
		if (!gameStarted)
		{
			return;
		}

		float h = Input.GetAxis("Horizontal");

		if (h < 0)
		{
			_OnTouchLeft();
		}
		else if (h > 0)
		{
			_OnTouchRight();
		}

#endif

#if (UNITY_ANDROID || UNITY_IOS || UNITY_TVOS) && !UNITY_EDITOR

		int nbTouches = Input.touchCount;

		if (nbTouches > 0)
		{
		    Touch touch = Input.GetTouch(0);
			TouchPhase phase = touch.phase;

			if (phase == TouchPhase.Began)
			{
				print("on touch");

				if (touch.position.x < Screen.width / 2f)
				{
					_OnTouchLeft();
				}
				else
				{
					_OnTouchRight();
				}
			}
		}
	
#endif

#if (!UNITY_ANDROID && !UNITY_IOS && !UNITY_TVOS) || UNITY_EDITOR

           if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                _OnTouchLeft();
            }

            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                _OnTouchRight();
            }

#endif

        }
        #endregion

        #region Private methods
        private void _OnTouchLeft()
        {
            if (OnTouchScreen != null)
            {
                OnTouchScreen();
            }

            if (OnTouchLeft != null)
            {
                OnTouchLeft();
            }
        }

        private void _OnTouchRight()
        {
            if (OnTouchScreen != null)
            {
                OnTouchScreen();
            }

            if (OnTouchRight != null)
            {
                OnTouchRight();
            }
        }
        #endregion
    }
}

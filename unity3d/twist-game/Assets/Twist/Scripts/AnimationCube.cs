﻿using UnityEngine;
using System.Collections;

namespace Twist
{
    public class AnimationCube : MonoBehaviour
    {
        #region Public resources
        public Transform cube;
        #endregion

        #region Serialized fields
        [SerializeField]
        private Vector3 position;
        #endregion

        #region Private resources
        private float animTime = 2;
        #endregion

        #region System metods
        private void Awake()
        {
            position = cube.localPosition;

            cube.gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            StopAllCoroutines();

            cube.gameObject.SetActive(false);
        }

        private void OnDisable()
        {
            cube.gameObject.SetActive(false);

            StopAllCoroutines();
        }
        #endregion

        #region Public methods
        public void DoPosition()
        {
            cube.gameObject.SetActive(false);

            StopAllCoroutines();
            StartCoroutine(_DoPosition());
        }
        #endregion

        #region Private methods
        private IEnumerator _DoPosition()
        {
            Vector3 startPosition = new Vector3(position.x * 50, position.y * 50, 0);
            cube.localPosition = startPosition;
            cube.gameObject.SetActive(true);
            Vector3 finalPosition = position;
            float timer = 0;

            while (timer <= animTime)
            {
                timer += Time.deltaTime;
                var posTemp = Vector3.Lerp(startPosition, finalPosition, timer / animTime);
                cube.localPosition = posTemp;

                yield return null;
            }
        }
        #endregion
    }
}

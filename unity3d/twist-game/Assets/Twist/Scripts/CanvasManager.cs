﻿using UnityEngine;
using UnityEngine.UI;

#if !UNITY_5_0 || !UNITY_5_1 || !UNITY_5_2 

using UnityEngine.SceneManagement;

#endif

using System.Collections;

namespace Twist
{
    public class CanvasManager : MonoBehaviorHelper
    {
        #region Serialized fields
        [SerializeField]
        private int m_point;
        [SerializeField]
        private Text score;
        [SerializeField]
        private Text lastScoreGameOver;
        [SerializeField]
        private Text bestScoreGameOver;
        [SerializeField]
        private Text textStart;
        [SerializeField]
        private Button buttonRestart;
        [SerializeField]
        private Button buttonFreeLife;
        [SerializeField]
        private Button buttonContinueYES;
        [SerializeField]
        private Button buttonContinueNO;
        [SerializeField]
        private GameObject scoreGameOverParent;
        [SerializeField]
        private CanvasGroup menuCanvasGroup;
        [SerializeField]
        private CanvasGroup continueCanvasGroup;
        [SerializeField]
        private Text continueText;
        #endregion

        #region System methods
        private void Awake()
        {
            m_point = 0;
            score.text = m_point.ToString();

            if (menuCanvasGroup == null)
            {
                menuCanvasGroup = transform.FindChild("Menu").GetComponent<CanvasGroup>();
            }

            if (score == null)
            {
                score = transform.FindChild("Score").GetComponent<Text>();
            }

            if (buttonRestart == null)
            {
                buttonRestart = transform.FindChild("Menu").FindChild("ButtonRestart").GetComponent<Button>();
            }

            if (scoreGameOverParent == null)
            {
                scoreGameOverParent = transform.FindChild("Menu").FindChild("scoreGameOverParent").gameObject;
            }

            if (lastScoreGameOver == null)
            {
                lastScoreGameOver = transform.FindChild("ScoreGameOverParent").FindChild("LastScore").GetComponent<Text>();
            }

            if (bestScoreGameOver == null)
            {
                bestScoreGameOver = transform.FindChild("ScoreGameOverParent").FindChild("BestScore").GetComponent<Text>();
            }

            if (textStart == null)
            {
                textStart = transform.FindChild("Menu").FindChild("TextStart").GetComponent<Text>();
            }
        }

        private void OnEnable()
        {
            InputTouch.OnTouchLeft += OnTouch;
            InputTouch.OnTouchRight += OnTouch;
            GameManager.OnGameEnded += OnGameEnded;
        }

        private void OnDisable()
        {
            InputTouch.OnTouchLeft -= OnTouch;
            InputTouch.OnTouchRight -= OnTouch;
            GameManager.OnGameEnded -= OnGameEnded;
        }

        private IEnumerator Start()
        {
            textStart.gameObject.SetActive(true);
            score.gameObject.SetActive(false);
            lastScoreGameOver.gameObject.SetActive(false);
            bestScoreGameOver.gameObject.SetActive(false);
            buttonRestart.gameObject.SetActive(false);
            buttonFreeLife.gameObject.SetActive(false);
            scoreGameOverParent.SetActive(false);
            continueCanvasGroup.gameObject.SetActive(false);

            float alpha = 0f;
            menuCanvasGroup.alpha = alpha;

            if (Application.isMobilePlatform)
            {
                textStart.text = "Tap to begin";
            }
            else
            {
                textStart.text = "Tap any key to begin";
            }

            yield return new WaitForSeconds(2);

            while (true)
            {
                alpha += 0.03f;
                menuCanvasGroup.alpha = alpha;

                if (alpha >= 1)
                {
                    break;
                }

                yield return null;
            }

            menuCanvasGroup.alpha = 1f;

            buttonRestart.onClick.AddListener(() =>
            {
                AdsManager.instance.ShowAdsGameOver();
                buttonRestart.onClick.RemoveAllListeners();

#if !UNITY_5_0 || !UNITY_5_1 || !UNITY_5_2

                SceneManager.LoadScene("Twist");

#else

			Application.LoadLevel (Application.loadedLevel);

#endif

            });

            buttonFreeLife.onClick.AddListener(() =>
            {
                player.OnGameEnded();

                AdsManager.instance.ShowRewardedVideoGameOver((bool success) =>
                {
                    print("rewarded video success ? " + success);

                    if (success)
                    {
                        int l = gameManager.GetPlayerLife();
                        l += 3;
                        gameManager.SetPlayerLife(l);
                    }

                    ShowMenuGameOver();
                    player.OnGameEnded();
                });
            });
        }
        #endregion

        #region Public methods
        public void Add1Point()
        {
            m_point++;
            score.text = m_point.ToString();
        }
        #endregion

        #region Private methods
        private void OnTouch()
        {
            if (menuCanvasGroup.alpha != 1f)
            {
                return;
            }

            if (Input.anyKeyDown || Input.GetMouseButton(0))
            {
                score.gameObject.SetActive(true);
                StartCoroutine(DoFadeOut());
            }
        }

        private void OnGameEnded()
        {
            ShowMenuGameOver();

#if UNITY_TVOS

		FindObjectOfType<InputTouch>().OnGameOver();

#endif

        }

        private IEnumerator DoFadeOut()
        {
            float alpha = 1f;
            menuCanvasGroup.alpha = alpha;

            while (true)
            {
                alpha -= 0.03f;
                menuCanvasGroup.alpha = alpha;

                if (alpha <= 0)
                {
                    break;
                }

                yield return null;
            }

            menuCanvasGroup.alpha = 0f;
            gameManager.OnGameStart();
        }

        private IEnumerator DoFadeIn()
        {
            float alpha = 0f;
            menuCanvasGroup.alpha = alpha;

            while (true)
            {
                alpha += 0.03f;
                menuCanvasGroup.alpha = alpha;

                if (alpha >= 1)
                {
                    break;
                }

                yield return null;
            }

            menuCanvasGroup.alpha = 1f;

#if UNITY_TVOS

		FindObjectOfType<InputTouch>().OnGameOver();

#endif

        }

        private IEnumerator DoFadeInContinue()
        {
            float alpha = 0f;
            continueCanvasGroup.alpha = alpha;

            while (true)
            {
                alpha += 0.03f;
                continueCanvasGroup.alpha = alpha;

                if (alpha >= 1)
                {
                    break;
                }

                yield return null;
            }

            continueCanvasGroup.alpha = 1f;
        }

        private IEnumerator DoFadeOutContinue()
        {
            float alpha = 1f;
            continueCanvasGroup.alpha = alpha;

            while (true)
            {
                alpha -= 0.03f;
                continueCanvasGroup.alpha = alpha;

                if (alpha <= 0)
                {
                    break;
                }

                yield return null;
            }

            continueCanvasGroup.alpha = 0f;
        }
      
        private void OnCLickedContinueYES()
        {
            buttonContinueYES.onClick.RemoveAllListeners();
            buttonContinueNO.onClick.RemoveAllListeners();

            StartCoroutine(DoFadeOutContinue());

            textStart.gameObject.SetActive(true);
            score.gameObject.SetActive(false);
            lastScoreGameOver.gameObject.SetActive(false);
            bestScoreGameOver.gameObject.SetActive(false);
            buttonRestart.gameObject.SetActive(false);
            buttonFreeLife.gameObject.SetActive(false);
            scoreGameOverParent.SetActive(false);

            StartCoroutine(DoFadeIn());

            gameManager.UseOneLife();

            player.RestartPlayerFromContinue();
        }

        private void OnCLickedContinueNO()
        {
            buttonContinueYES.onClick.RemoveAllListeners();
            buttonContinueNO.onClick.RemoveAllListeners();

            StartCoroutine(DoFadeOutContinue());

            textStart.gameObject.SetActive(true);
            score.gameObject.SetActive(false);
            lastScoreGameOver.gameObject.SetActive(false);
            bestScoreGameOver.gameObject.SetActive(false);
            buttonRestart.gameObject.SetActive(false);
            buttonFreeLife.gameObject.SetActive(false);
            scoreGameOverParent.SetActive(false);

            _ShowMenuGameOver();
        }

        private void ShowMenuGameOver()
        {

#if UNITY_IOS || UNITY_ANDROID || UNITY_EDITOR

            if (m_point > 2 && gameManager.GetPlayerLife() > 0)
            {
                player.OnGameEnded();

                continueText.text = "YOU HAVE " + gameManager.GetPlayerLife().ToString() + " LIFE" + " \n\nCONTINUE?";
                textStart.gameObject.SetActive(false);
                score.gameObject.SetActive(false);
                lastScoreGameOver.gameObject.SetActive(false);
                bestScoreGameOver.gameObject.SetActive(false);
                buttonRestart.gameObject.SetActive(false);
                buttonFreeLife.gameObject.SetActive(false);
                scoreGameOverParent.SetActive(false);

                continueCanvasGroup.gameObject.SetActive(true);

                StartCoroutine(DoFadeInContinue());

                buttonContinueYES.onClick.RemoveAllListeners();
                buttonContinueNO.onClick.RemoveAllListeners();

                buttonContinueYES.onClick.AddListener(OnCLickedContinueYES);
                buttonContinueNO.onClick.AddListener(OnCLickedContinueNO);

                player.OnGameEnded();

                return;
            }

#endif

            _ShowMenuGameOver();
        }
        
        private void _ShowMenuGameOver()
        {
            Score.SaveLastScore(m_point);
            int bestScore = Score.LoadBestScore();

            lastScoreGameOver.text = m_point.ToString();

            bestScoreGameOver.text = bestScore.ToString();

            continueCanvasGroup.gameObject.SetActive(false);
            textStart.gameObject.SetActive(false);
            score.gameObject.SetActive(false);
            lastScoreGameOver.gameObject.SetActive(true);
            bestScoreGameOver.gameObject.SetActive(true);
            buttonRestart.gameObject.SetActive(true);
            buttonFreeLife.gameObject.SetActive(AdsManager.instance.RewardedVideoIsInitialized());
            scoreGameOverParent.SetActive(true);

#if UNITY_TVOS

		FindObjectOfType<InputTouch>().OnGameOver();

#endif

            StartCoroutine(DoFadeIn());
        }
        #endregion
    }
}
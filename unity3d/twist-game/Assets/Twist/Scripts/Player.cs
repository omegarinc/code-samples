﻿using UnityEngine;
using System.Collections;
using System;

namespace Twist
{
    public class Player : MonoBehaviorHelper
    {
        #region Pubic resources
        public float speedRotate = 50f;
        public float speedJump = 50f;
        public Transform groundCheck;
        public Vector3 originalPosition = new Vector3(0, -3, 4.371f);
        public Transform transformParent;
        public Transform sphere;
        public Transform lastGoodCube;
        public GameObject shadow;
        public MeshRenderer sphereRenderer;
        #endregion

        #region Private resources
        private float currentDegree = 0;
        private bool isJumping;
        private bool isGameOver;
        private bool isStarted;
        private float scrollSpeed = 1.5F;
        private float animTime = 0.2f;
        private RaycastHit hit;
        #endregion

        #region System methods
        private void Awake()
        {
            groundCheck = transform.FindChild("GroundCheck");
            currentDegree = 0;
            isJumping = false;
            isGameOver = false;
            isStarted = false;
        }

        private void Update()
        {
            transform.localPosition = originalPosition;

            if (!isStarted)
            {
                return;
            }

            DoDextureOffset();

            if (!isGrounded() && !isJumping && !isGameOver)
            {
                soundManager.PlaySoundFail();
                isGameOver = true;
                StartCoroutine(DoAnimGamOver());

                return;
            }
        }

        private void OnEnable()
        {
            InputTouch.OnTouchLeft += MoveLeft;
            InputTouch.OnTouchRight += MoveRight;
            GameManager.OnGameStarted += OnGameStarted;
            GameManager.OnGameEnded += OnGameEnded;
        }

        private void OnDisable()
        {
            InputTouch.OnTouchLeft -= MoveLeft;
            InputTouch.OnTouchRight -= MoveRight;
            GameManager.OnGameStarted -= OnGameStarted;
            GameManager.OnGameEnded -= OnGameEnded;
        }
        #endregion

        #region Public methods
        public void OnGameEnded()
        {
            isJumping = false;
            isGameOver = true;
            StopAllCoroutines();
        }

        public void RestartPlayerFromContinue()
        {
            isJumping = false;
            isGameOver = false;
            isStarted = false;
            transform.localPosition = originalPosition;
            var pos = transformParent.position;
            pos.z = lastGoodCube.position.z - 4.69f - 0.3f;
            transformParent.position = pos;
            transformParent.rotation = lastGoodCube.rotation;
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            sphere.localPosition = new Vector3(0, -0.24f, 0);
        }
        #endregion

        #region Private methods
        private void OnGameStarted()
        {
            isStarted = true;
        }

        private void DoDextureOffset()
        {
            if (isGameOver)
            {
                return;
            }

            float offset = Time.time * scrollSpeed;
            sphereRenderer.material.SetTextureOffset("_MainTex", -new Vector2(offset, 0));
        }

        private bool PlayerCanJump()
        {
            if (!isStarted)
            {
                return false;
            }

            if (!isGrounded() && !isJumping && !isGameOver)
            {
                return false;
            }

            if (isJumping || isGameOver)
            {
                return false;
            }

            return true;
        }
        
        private void MoveLeft()
        {
            if (PlayerCanJump())
            {
                DoMove(-1);
            }
        }

        private void MoveRight()
        {
            if (PlayerCanJump())
            {
                DoMove(+1);
            }
        }

        private void DoMove(int sign)
        {
            soundManager.PlaySoundJump();
            isJumping = true;
            currentDegree += sign * 45f;

            StartCoroutine(RotateTo(sign));
            StartCoroutine(DoJump());
        }

        private IEnumerator DoJump()
        {
            isJumping = true;
            shadow.SetActive(false);

            float startPosY = -0.24f;
            float finalPosY = -0.24f + 1f;
            float timer = 0;
            float timeJump = animTime / 2f;

            while (timer <= timeJump)
            {
                timer += Time.deltaTime;
                float yPosTemp = 0;
                yPosTemp = Mathf.Lerp(startPosY, finalPosY, timer / timeJump);
                var s = sphere.localPosition;
                s.y = yPosTemp;
                sphere.localPosition = s;

                yield return null;
            }

            timer = 0;

            while (timer <= timeJump)
            {
                timer += Time.deltaTime;
                float yPosTemp = 0;
                yPosTemp = Mathf.Lerp(finalPosY, startPosY, timer / timeJump);
                var s = sphere.localPosition;
                s.y = yPosTemp;
                sphere.localPosition = s;

                yield return null;
            }
        }

        private IEnumerator RotateTo(float direction)
        {
            isJumping = true;
            shadow.SetActive(false);
            float originalRotation = transformParent.eulerAngles.z;
            float finalRotation = originalRotation + direction * 45f;
            float timer = 0;

            while (timer <= animTime)
            {
                timer += Time.deltaTime;
                float rotTemp = Mathf.Lerp(originalRotation, finalRotation, timer / animTime);
                transformParent.eulerAngles = Vector3.forward * rotTemp;
                yield return null;
            }
            
            if (isGrounded(true))
            {
                canvasManager.Add1Point();
            }

            isJumping = false;

            yield return 0;

            shadow.SetActive(true);
        }

        private IEnumerator DoAnimGamOver()
        {
            isJumping = false;
            isGameOver = true;
            float startPosY = -0.24f;
            float finalPosY = -0.24f - 5;
            float timer = 0;
            float timeJump = animTime;

            while (timer <= timeJump)
            {
                timer += Time.deltaTime;
                float yPosTemp = 0;
                yPosTemp = Mathf.Lerp(startPosY, finalPosY, timer / timeJump);
                var s = sphere.localPosition;
                s.y = yPosTemp;
                sphere.localPosition = s;

                yield return null;
            }

            gameManager.OnGameEnd();
        }

        private bool isGrounded()
        {
            return isGrounded(true);
        }

        private bool isGrounded(bool savePos)
        {
            if (isJumping && !savePos)
            {
                return true;
            }

            Vector3 down = transform.TransformDirection(Vector3.down);

            if (Physics.Raycast(groundCheck.position, down, out hit, 10))
            {
                if (savePos)
                {
                    if (!isJumping)
                    {
                        var t = hit.transform;
                        var tt = t.GetComponentInParent<AnimationCube>();

                        if (tt != null)
                        {
                            lastGoodCube = tt.cube;
                        }
                    }
                }

                return true;
            }

            return false;
        }
        #endregion
    }
}
﻿using UnityEngine;
using System.Collections;

namespace Twist
{
    public class MonoBehaviorHelper : MonoBehaviour
    {
        #region Fields
        private GameManager _gameManager;
        public GameManager gameManager
        {
            get
            {
                if (_gameManager == null)
                    _gameManager = FindObjectOfType<GameManager>();

                return _gameManager;
            }
        }

        private Player _player;
        public Player player
        {
            get
            {
                if (_player == null)
                    _player = FindObjectOfType<Player>();

                return _player;
            }
        }

        private CanvasManager _canvasManager;
        public CanvasManager canvasManager
        {
            get
            {
                if (_canvasManager == null)
                    _canvasManager = FindObjectOfType<CanvasManager>();

                return _canvasManager;
            }
        }

        private SoundManager _soundManager;
        public SoundManager soundManager
        {
            get
            {
                if (_soundManager == null)
                    _soundManager = FindObjectOfType<SoundManager>();

                return _soundManager;
            }
        }
        #endregion
    }
}

﻿using UnityEngine;
using System.Collections;

namespace Twist
{
    public class Destroyer : MonoBehaviour
    {
        #region System methods
        private void OnTriggerEnter(Collider other)
        {
            other.transform.parent.gameObject.SetActive(false);
        }
        #endregion
    }
}

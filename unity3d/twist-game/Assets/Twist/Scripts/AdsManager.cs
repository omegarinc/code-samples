﻿#pragma warning disable 0168 // variable declared but not used.
#pragma warning disable 0219 // variable assigned but not used.
#pragma warning disable 0414 // private field assigned but not used.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.Events;
using UnityEngine.EventSystems;

#if UNITY_ADS

using UnityEngine.Advertisements;

#endif

#if CHARTBOOST

using ChartboostSDK;

#endif

#if UNITY_IOS

using ADBannerView = UnityEngine.iOS.ADBannerView;
using ADInterstitialAd = UnityEngine.iOS.ADInterstitialAd;

#endif

#if GOOGLE_MOBILE_ADS

using GoogleMobileAds;
using GoogleMobileAds.Api;

#endif

public class AdsManager : MonoBehaviour 
{
	private static AdsManager _instance;

	public static AdsManager instance
	{
		get {
			if (_instance == null)
			{
				_instance = FindObjectOfType(typeof(AdsManager)) as AdsManager;
			}
			return _instance;
		}
	} 

	public bool rewardedVideoAlwaysReadyInSimulator = true;
	public bool rewardedVideoAlwaysSuccessInSimulator = true;
	public bool basedTimeInterstitialAtGameOver = false;
	public int numberOfPlayToShowInterstitial = 5;
	public float numberOfMinutesToShowAnInterstitialAtGameOver = 2;
	public bool ShowIntertitialAtStart = true;
	public bool NO_ADS = false;

    private float realTimeSinceStartup;

#if UNITY_IOS && (GOOGLE_MOBILE_ADS || STAN_ASSET_GOOGLEMOBILEADS || STAN_ASSET_ANDROIDNATIVE)

	public bool useAdmob;

#endif

#if GOOGLE_MOBILE_ADS

	BannerView bannerView;
	AdRequest requestBanner;
	InterstitialAd interstitial;
	AdRequest requestInterstitial;

#endif

#if UNITY_IOS

	private ADBannerView banner = null;
	private ADInterstitialAd fullscreenAd = null;

#endif

#if STAN_ASSET_GOOGLEMOBILEADS || STAN_ASSET_ANDROIDNATIVE || GOOGLE_MOBILE_ADS

	public string AdmobBannerIdIOS = "ca-app-pub-4501064062171971/5799911242";
	public string AdmobInterstitialIdIOS = "ca-app-pub-4501064062171971/7276644447";
	public string AdmobBannerIdANDROID = "ca-app-pub-4501064062171971/9943794444";
	public string AdmobInterstitialIdANDROID = "ca-app-pub-4501064062171971/2420527649";

#endif

#if STAN_ASSET_GOOGLEMOBILEADS

	private static Dictionary<string, GoogleMobileAdBanner> _registerdBanners = null;

#endif

#if STAN_ASSET_ANDROIDNATIVE

	private static Dictionary<string, GoogleMobileAdBanner> _registerdBanners = null;

#endif

#if UNITY_ADS

	public string rewardedVideoZoneUnityAds = "rewardedVideoZone";

#endif

#if ADCOLONY

	// Arbitrary version number
	public string version = "1.1";
	public string ADCOLONY_appId
	{
		get
		{

#if UNITY_IOS

			return AdColonyAppID_iOS;

#endif

#if UNITY_ANDROID

	        return AdColonyAppID_ANDROID;

#endif

		}
	}

	public string ADCOLONY_RewardedVideoZoneID
	{
		get
		{

#if UNITY_IOS

			return AdColonyRewardedVideoZoneID_iOS;

#endif

#if UNITY_ANDROID

	        return AdColonyRewardedVideoZoneID_ANDROID;

#endif

		}
	}

	public string ADCOLONY_RewardedVideoZoneKEY
	{
		get
		{

#if UNITY_IOS

			return AdColonyRewardedVideoZoneKEY_iOS;

#endif

#if UNITY_ANDROID

	        return AdColonyRewardedVideoZoneKEY_ANDROID;

#endif

		}
	}

	public string ADCOLONY_InterstitialVideoZone1KEY
	{
		get
		{

#if UNITY_IOS

			return AdColonyInterstitialVideoZone1KEY_iOS;
#endif

#if UNITY_ANDROID

	        return AdColonyInterstitialVideoZone1KEY_ANDROID;

#endif

		}
	}

	public string ADCOLONY_InterstitialVideoZone2KEY
	{
		get
		{

#if UNITY_IOS

			return AdColonyInterstitialVideoZone2KEY_iOS;

#endif

#if UNITY_ANDROID

	        return AdColonyInterstitialVideoZone2KEY_ANDROID;

#endif

		}
	}

	public string ADCOLONY_InterstitialVideoZone1ID
	{
		get
		{

#if UNITY_IOS

			return AdColonyInterstitialVideoZone1ID_iOS;

#endif

#if UNITY_ANDROID

	        return AdColonyInterstitialVideoZone1ID_ANDROID;

#endif

		}
	}

	public string ADCOLONY_InterstitialVideoZone2ID
	{
		get
		{

#if UNITY_IOS

			return AdColonyInterstitialVideoZone2ID_iOS;

#endif

#if UNITY_ANDROID

	        return AdColonyInterstitialVideoZone2ID_ANDROID;

#endif
		}
	}

	[SerializeField]
    private string AdColonyAppID_iOS = "app4901e09bc6a84e2e90";
	[SerializeField]
    private string AdColonyInterstitialVideoZone1KEY_iOS = "VideoZone1";
	[SerializeField]
    private string AdColonyInterstitialVideoZone1ID_iOS = "vzd728fbab1a2948498e";
	[SerializeField]
    private string AdColonyInterstitialVideoZone2KEY_iOS = "VideoZone2";
	[SerializeField]
    private string AdColonyInterstitialVideoZone2ID_iOS = "vz0042762cf2c04f5ba6";
	[SerializeField]
    private string AdColonyRewardedVideoZoneKEY_iOS = "V4VCZone1";
	[SerializeField]
    private string AdColonyRewardedVideoZoneID_iOS = "vzbfea02bb641f4a6a95";
	[SerializeField]
    private string AdColonyAppID_ANDROID = "app4901e09bc6a84e2e90";
	[SerializeField]
    private string AdColonyInterstitialVideoZone1KEY_ANDROID = "VideoZone1";
	[SerializeField]
    private string AdColonyInterstitialVideoZone1ID_ANDROID = "vzaed6b7d9b27b4fc3b9";
	[SerializeField]
    private string AdColonyInterstitialVideoZone2KEY_ANDROID = "VideoZone2";
	[SerializeField]
    private string AdColonyInterstitialVideoZone2ID_ANDROID = "vz4337825b0684481da8";
	[SerializeField]
    private string AdColonyRewardedVideoZoneKEY_ANDROID = "V4VCZone1";
	[SerializeField]
    private string AdColonyRewardedVideoZoneID_ANDROID = "vza17248a990054b17ae";
	[SerializeField]
    private string AdColonyRewardedVideoSecretKey_ANDROID = "v4vc39381a8b790943c4bd";

#endif

    private void Awake()
	{
		if (!Application.isEditor)
		{
			rewardedVideoAlwaysReadyInSimulator = false;
			rewardedVideoAlwaysSuccessInSimulator = false;
		}

		Set();

		DontDestroyOnLoad(this.gameObject);
	}

	private IEnumerator Start()
	{
		yield return 0;

		GC.Collect();

		Resources.UnloadUnusedAssets ();

		Application.targetFrameRate = 60;

		if (!NO_ADS)
		{
			#if UNITY_IOS && (STAN_ASSET_GOOGLEMOBILEADS || STAN_ASSET_ANDROIDNATIVE || GOOGLE_MOBILE_ADS)

			if (!useAdmob)
			{

			#endif

				#if UNITY_IOS

				fullscreenAd = new ADInterstitialAd();
				ADInterstitialAd.onInterstitialWasLoaded  += OnFullscreenLoaded;

				#if !UNITY_4_6 && !UNITY_5_0 && !UNITY_5_1

				ADInterstitialAd.onInterstitialWasViewed  += OnFullscreenViewed;

				#endif

				#endif

				#if UNITY_IOS && (STAN_ASSET_GOOGLEMOBILEADS || STAN_ASSET_ANDROIDNATIVE || GOOGLE_MOBILE_ADS)

			}

				#endif

			#if STAN_ASSET_GOOGLEMOBILEADS

			if (!GoogleMobileAd.IsInited)
            {
				GoogleMobileAd.Init();
			}

			#endif

			#if STAN_ASSET_ANDROIDNATIVE

			if (!AndroidAdMobController.Instance.IsInited)
            {
				Set();
			}

			#endif

			#if CHARTBOOST

			Chartboost.setAutoCacheAds(true);
			Chartboost.cacheInterstitial(CBLocation.Default);
			Chartboost.cacheRewardedVideo(CBLocation.Default);

			#endif

			yield return new WaitForSeconds(1);

			ShowBanner ();

			yield return new WaitForSeconds(1);

			if (ShowIntertitialAtStart)
			{
				_ShowInterstitial();
			}
		}

	}

	#if GOOGLE_MOBILE_ADS

	private void RequestInterstitial()
	{
		if (Application.isMobilePlatform) 
		{
			requestInterstitial = new AdRequest.Builder().Build();
			interstitial.LoadAd(requestInterstitial);
		}
	}
	#endif

	public void Set()
	{
		realTimeSinceStartup = Time.realtimeSinceStartup;

		#if GOOGLE_MOBILE_ADS

		if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
			bannerView = new BannerView(AdmobBannerIdIOS, AdSize.SmartBanner, AdPosition.Bottom);
        }
		else if (Application.platform == RuntimePlatform.Android)
        {
			bannerView = new BannerView(AdmobBannerIdANDROID, AdSize.SmartBanner, AdPosition.Bottom);
        }

		requestBanner = new AdRequest.Builder().Build();

		if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
			interstitial = new InterstitialAd(AdmobInterstitialIdIOS);
        }
		else if (Application.platform == RuntimePlatform.Android)
        {
			interstitial = new InterstitialAd(AdmobInterstitialIdANDROID);
        }

		RequestInterstitial();

		#endif

		#if STAN_ASSET_GOOGLEMOBILEADS

			GoogleMobileAdSettings.Instance.IOS_BannersUnitId = AdmobBannerIdIOS;
			GoogleMobileAdSettings.Instance.IOS_InterstisialsUnitId = AdmobInterstitialIdIOS;
			GoogleMobileAdSettings.Instance.Android_BannersUnitId = AdmobBannerIdANDROID;
			GoogleMobileAdSettings.Instance.Android_InterstisialsUnitId = AdmobInterstitialIdANDROID;

		#endif

		#if STAN_ASSET_ANDROIDNATIVE

		if (!string.IsNullOrEmpty(AdmobBannerIdANDROID))
        {
			AndroidAdMobController.Instance.SetBannersUnitID(AdmobBannerIdANDROID);
        }

		if (!string.IsNullOrEmpty(AdmobInterstitialIdANDROID))
        {
			AndroidAdMobController.Instance.SetInterstisialsUnitID(AdmobInterstitialIdANDROID);
        }

		if (!string.IsNullOrEmpty(AdmobBannerIdANDROID) && !string.IsNullOrEmpty(AdmobInterstitialIdANDROID))
        {
			AndroidAdMobController.Instance.Init (AdmobBannerIdANDROID,AdmobInterstitialIdANDROID);
        }
		else if (!string.IsNullOrEmpty(AdmobBannerIdANDROID))
        {
			AndroidAdMobController.Instance.Init (AdmobBannerIdANDROID);
        }

		#endif

		#if CHARTBOOST

		var c = FindObjectOfType<Chartboost>();

		if (c == null)
		{
		    gameObject.AddComponent<Chartboost>();
		}

		#endif

	}

	#if GOOGLE_MOBILE_ADS

	private void HandleAdLoaded(object sender, EventArgs e)
	{

	}

	private void HandleAdFailedToLoad(object sender, EventArgs e)
	{
		Invoke("ShowBanner", 10);
	}

	private void HandleAdOpened(object sender, EventArgs e)
	{

	}

	private void HandleAdClosing(object sender, EventArgs e)
	{

	}

	private void HandleAdClosed(object sender, EventArgs e)
	{

	}

	private void HandleAdLeftApplication(object sender, EventArgs e)
	{

	}

	public void Show_Banner()
	{
		if (bannerView != null)
        {
			bannerView.Show();
        }
	}

	public void Hide_Banner()
	{
		if (bannerView != null)
        {
			bannerView.Hide();
        }
	}
	#endif
    
	public void ShowBanner() 
	{
        if (NO_ADS)
        {
            return;
        }

		#if UNITY_IOS

		#if STAN_ASSET_GOOGLEMOBILEADS || STAN_ASSET_ANDROIDNATIVE || GOOGLE_MOBILE_ADS

		if (!useAdmob)
		{

		#endif

			ShowIAdBanner();

		#if STAN_ASSET_GOOGLEMOBILEADS || STAN_ASSET_ANDROIDNATIVE || GOOGLE_MOBILE_ADS

		}

		#endif

		#endif

		#if UNITY_IOS

		#if STAN_ASSET_GOOGLEMOBILEADS || STAN_ASSET_ANDROIDNATIVE || GOOGLE_MOBILE_ADS

		if (useAdmob)
		{

		#endif

		#endif

			#if GOOGLE_MOBILE_ADS

			if (Application.isMobilePlatform)
			{
				bannerView.LoadAd(requestBanner);
				bannerView.Show();

				bannerView.AdLoaded -= HandleAdLoaded;
				bannerView.AdFailedToLoad -= HandleAdFailedToLoad;
				bannerView.AdOpened -= HandleAdOpened;
				bannerView.AdClosing -= HandleAdClosing;
				bannerView.AdClosed -= HandleAdClosed;
				bannerView.AdLeftApplication -= HandleAdLeftApplication;
				bannerView.AdLoaded += HandleAdLoaded;
				bannerView.AdFailedToLoad += HandleAdFailedToLoad;
				bannerView.AdOpened += HandleAdOpened;
				bannerView.AdClosing += HandleAdClosing;
				bannerView.AdClosed += HandleAdClosed;
				bannerView.AdLeftApplication += HandleAdLeftApplication;
			}

			#endif

			#if UNITY_IOS

			#if STAN_ASSET_GOOGLEMOBILEADS || STAN_ASSET_ANDROIDNATIVE || GOOGLE_MOBILE_ADS

		}

			#endif

			#endif

		#if STAN_ASSET_GOOGLEMOBILEADS

		if (!GoogleMobileAd.IsInited)
		{
			GoogleMobileAd.Init();
		}  
		else 
		{
			GoogleMobileAdBanner banner;

			if (registerdBanners.ContainsKey(sceneBannerId))
			{
				banner = registerdBanners[sceneBannerId];
			}  
			else 
			{
				banner = GoogleMobileAd.CreateAdBanner(TextAnchor.LowerCenter, GADBannerSize.SMART_BANNER);

				registerdBanners.Add(sceneBannerId, banner);
			}

			if (banner.IsLoaded && !banner.IsOnScreen) 
			{
				banner.Show ();
			}
		}

		#endif

		#if STAN_ASSET_ANDROIDNATIVE

		if (!AndroidAdMobController.Instance.IsInited)
		{
			//AndroidAdMobController.Init();

			Set();
		}  
		else 
		{
			GoogleMobileAdBanner banner;

			if (registerdBanners.ContainsKey(sceneBannerId))
			{
				banner = registerdBanners [sceneBannerId];
			}  
			else 
			{
				banner = AndroidAdMobController.Instance.CreateAdBanner(TextAnchor.LowerCenter, GADBannerSize.SMART_BANNER);

				registerdBanners.Add(sceneBannerId, banner);
			}

			if (banner.IsLoaded && !banner.IsOnScreen) 
			{
				banner.Show();
			}
		}
		#endif
	}

	private void ShowIAdBanner()
	{
        if (NO_ADS)
        {
            return;
        }

		#if UNITY_IOS

		CancelInvoke("ShowIAdBanner");

		#if STAN_ASSET_GOOGLEMOBILEADS || STAN_ASSET_ANDROIDNATIVE || GOOGLE_MOBILE_ADS

		if (!useAdmob)
		{

		#endif

			if (banner == null)
			{
				banner = new ADBannerView(ADBannerView.Type.Banner, ADBannerView.Layout.BottomCenter);
				banner.visible = true;

				ADBannerView.onBannerWasClicked -= OnBannerClicked;
				ADBannerView.onBannerWasLoaded -= OnBannerLoaded;
				ADBannerView.onBannerWasClicked += OnBannerClicked;
				ADBannerView.onBannerWasLoaded += OnBannerLoaded;

		#if !UNITY_4_6 && !UNITY_5_0 && !UNITY_5_1

				ADBannerView.onBannerFailedToLoad -= OnBannerFailedToLoad;
				ADBannerView.onBannerFailedToLoad += OnBannerFailedToLoad;

		#endif

			}
			else
			{
				banner.visible = true;
			}

		#if STAN_ASSET_GOOGLEMOBILEADS || STAN_ASSET_ANDROIDNATIVE || GOOGLE_MOBILE_ADS

		}

		#endif

		#endif
	}
    
	public void HideBanner() 
	{
		#if STAN_ASSET_GOOGLEMOBILEADS

		if (registerdBanners.ContainsKey(sceneBannerId)) 
		{
			GoogleMobileAdBanner banner = registerdBanners[sceneBannerId];

			if (banner.IsLoaded) 
			{
				if (banner.IsOnScreen) 
				{
					banner.Hide();
				}
			}  
			else
			{
				banner.ShowOnLoad = false;
			}
		}

		#endif

		#if STAN_ASSET_ANDROIDNATIVE

		if (registerdBanners.ContainsKey(sceneBannerId)) 
		{
			GoogleMobileAdBanner banner = registerdBanners[sceneBannerId];

			if (banner.IsLoaded) 
			{
				if (banner.IsOnScreen) 
				{
					banner.Hide();
				}
			}  
			else
			{
				banner.ShowOnLoad = false;
			}
		}

		#endif
	}

	#if STAN_ASSET_GOOGLEMOBILEADS

	public static Dictionary<string, GoogleMobileAdBanner> registerdBanners 
	{
		get
		{
			if (_registerdBanners == null) 
			{
				_registerdBanners = new Dictionary<string, GoogleMobileAdBanner>();
			}

			return _registerdBanners;
		}
	}

	#endif

	#if STAN_ASSET_ANDROIDNATIVE

	public static Dictionary<string, GoogleMobileAdBanner> registerdBanners 
	{
		get
		{
			if (_registerdBanners == null) 
			{
				_registerdBanners = new Dictionary<string, GoogleMobileAdBanner>();
			}

			return _registerdBanners;
		}
	}

	#endif


	public string sceneBannerId 
	{
		get 
		{

#if !UNITY_5_0 || !UNITY_5_1 || !UNITY_5_2
            
            return UnityEngine.SceneManagement.SceneManager.GetActiveScene().name + "_" + this.gameObject.name;

#else

			return Application.loadedLevelName + "_" + this.gameObject.name;

#endif
            
        }
    }

	#if UNITY_IOS

	private void OnBannerClicked()
	{
		Debug.Log("OnBannerClicked!\n");
	}

	private void OnBannerLoaded()
	{
		Debug.Log("OnBannerLoaded!\n");

		banner.visible = true;
	}

	private void OnBannerFailedToLoad()
	{
		Debug.Log("OnBannerFailedToLoad!\n");

		banner.visible = false;

		Invoke("ShowIAdBanner",5);
	}

	private void OnFullscreenLoaded()
	{
		Debug.Log("AD Loaded\n");
	}

	private void OnFullscreenViewed()
	{
		Debug.Log("AD Viewed\n");

		fullscreenAd.ReloadAd();
	}

	private void WantToShowAD()
	{  

	#if STAN_ASSET_GOOGLEMOBILEADS || STAN_ASSET_ANDROIDNATIVE || GOOGLE_MOBILE_ADS

		if(useAdmob)
        {
			return;
        }

	#endif

		print("WantToShowAD");

		if (fullscreenAd == null)
		{
			print("iOS fullscreenAd is null, creating...");

			fullscreenAd = new ADInterstitialAd();
		}
		else
		{
			if (fullscreenAd.loaded)
			{
				print("iOS fullscreenAd.loaded = true, showing...");

				fullscreenAd.Show();
			}
			else
			{
				print("iOS fullscreenAd.loaded = false, reloading...");

				fullscreenAd.ReloadAd();
			}
		}
	}

	#endif

	public void ShowAdsGameOver()
	{
        if (NO_ADS)
        {
            return;
        }

		bool showAds = false;

		if (basedTimeInterstitialAtGameOver)
		{
			float t = Time.realtimeSinceStartup;
			float ourTIme = numberOfMinutesToShowAnInterstitialAtGameOver * 60;

			if ((realTimeSinceStartup - t) > ourTIme)
			{
				_ShowInterstitial();
				realTimeSinceStartup = t;
			}
		}
		else
		{
			int countPlay = PlayerPrefs.GetInt("numberOfPlayToShowInterstitial", 0);

			countPlay++;

			showAds = countPlay >= numberOfPlayToShowInterstitial;

			if (showAds) 
			{
				PlayerPrefs.SetInt("numberOfPlayToShowInterstitial", 0);
				PlayerPrefs.Save();
				_ShowInterstitial();
			}
			else 
			{
				PlayerPrefs.SetInt("numberOfPlayToShowInterstitial", countPlay);
				PlayerPrefs.Save();
			}

		}
	}

	private void _ShowInterstitial()
	{
        if (NO_ADS)
        {
            return;
        }

		#if CHARTBOOST

		int rand = UnityEngine.Random.Range(0, 2);

		switch(rand)
		{
		    case 0:
		        ShowAdmobInterstitialGameOver();
		        break;
		    case 1:
		        if (Chartboost.hasInterstitial(CBLocation.Default))
		        {
		            ShowChartboostInterstitialGameOver();
		        }
		        else
		        {
		            ShowAdmobInterstitialGameOver();
		        }
		        break;
		    default:
		        ShowAdmobInterstitialGameOver();
		        break;
		}

		#endif

		#if !CHARTBOOST

	    ShowAdmobInterstitialGameOver ();

		#endif
	}

	public bool RewardedVideoIsInitialized()
	{
		bool adsReady = false;

#if CHARTBOOST

		adsReady = adsReady || Chartboost.hasRewardedVideo(CBLocation.Default);

		if (!Chartboost.hasRewardedVideo(CBLocation.Default)) 
		{
		    Chartboost.cacheRewardedVideo (CBLocation.Default);
		}

#endif

#if ADCOLONY

		adsReady = adsReady || AdColony.IsVideoAvailable(ADCOLONY_RewardedVideoZoneID);

#endif

#if UNITY_ADS

		adsReady = adsReady || Advertisement.IsReady (rewardedVideoZoneUnityAds);

#endif

        if (Application.isEditor)
        {
            adsReady = rewardedVideoAlwaysReadyInSimulator;
        }

		return adsReady;
	}

	public void ShowRewardedVideoGameOver(Action<bool> success)
	{
		print("ShowRewardedVideoGameOver");

		if (Application.isEditor)
		{
            if (success != null)
            {
                success(rewardedVideoAlwaysSuccessInSimulator);
            }

			return;
		}

		bool showRewardedOK = false;

#if ADCOLONY

		showRewardedOK = ShowRewardedVideoGameOverADCOLONY(success);

		if (showRewardedOK)
        {
			return;
        }

#endif

#if CHARTBOOST

		showRewardedOK = ShowRewardedVideoGameOverCHARTBOOST(success);

		if (showRewardedOK)
        {
		    return;
        }

#endif

#if UNITY_ADS

		showRewardedOK = ShowRewardedVideoGameOverUNITYADS(success);

		if (showRewardedOK)
        {
			return;
        }

#endif

        if (success != null)
        {
            success(showRewardedOK);
        }
	}

	#if ADCOLONY

	private bool ShowRewardedVideoGameOverADCOLONY(Action<bool> success)
	{
		print("check if adcolony have a video");

		if (AdColony.IsVideoAvailable(ADCOLONY_RewardedVideoZoneID))
		{
			print("adcolony have a video");

			AdColony.OfferV4VC(true, ADCOLONY_RewardedVideoZoneID);
			AdColony.OnV4VCResult += delegate(bool successRewarded, string name, int amount)
            {
				if (success != null)
                {
					success(successRewarded);
                }
			};

			return true;
		}

		return false;
	}

	#endif

	#if CHARTBOOST

	private bool ShowRewardedVideoGameOverCHARTBOOST(Action<bool> success)
	{
	    if (Chartboost.hasRewardedVideo(CBLocation.Default)) 
	    {
	        Chartboost.showRewardedVideo(CBLocation.Default);

	        Chartboost.didCompleteRewardedVideo += delegate(CBLocation arg1, int arg2)
            {
	            Debug.Log("!!!!!! Chartboost didCompleteRewardedVideo at location : " + arg1.ToString());

	            if (success != null)
                {
	                success (true);
                }
	        };

	        Chartboost.didFailToLoadRewardedVideo += delegate(CBLocation arg1, CBImpressionError arg2)
            {
	            Debug.Log("!!!!!! Chartboost didFailToLoadRewardedVideo at location : " + arg1.ToString());

	            if (success != null)
                {
	                success(false);
                }
	        };

	        return true;
	    }

	    Chartboost.cacheRewardedVideo(CBLocation.Default);

	    return false;
	}

	#endif

	#if UNITY_ADS
	public bool ShowRewardedVideoGameOverUNITYADS(Action<bool> success)
	{
		if (Advertisement.IsReady(rewardedVideoZoneUnityAds))
		{
			Advertisement.Show(rewardedVideoZoneUnityAds, new ShowOptions 
			{
				resultCallback = result => {
				    if (result == ShowResult.Finished)
				    {
					    Debug.Log ("user finished unity ads ===> offer 1 coin");

						if (success != null)
                        {
					        success (true);
                        }   

					}
					else if (result == ShowResult.Failed)
					{
						Debug.Log ("unity ads failed : " + result.ToString ());

						if (success != null)
                        {
							success (false);
                        }
					}
					else if (result == ShowResult.Skipped)
					{
						Debug.Log ("unity ads Skipped: " + result.ToString ());

					    if (success != null)
                        {
							success(false);
                        }
				    }
				}
				});

			return true;
		}

		return false;
	}

	#endif

	private void ShowAdmobInterstitialGameOver()
	{

		#if UNITY_IOS

		#if STAN_ASSET_GOOGLEMOBILEADS || STAN_ASSET_ANDROIDNATIVE || GOOGLE_MOBILE_ADS

		if (!useAdmob)
		{

		#endif

			WantToShowAD();

		#if STAN_ASSET_GOOGLEMOBILEADS || STAN_ASSET_ANDROIDNATIVE || GOOGLE_MOBILE_ADS

		}

		#endif

		#endif

		#if GOOGLE_MOBILE_ADS

		#if UNITY_IOS

		if (useAdmob)
		{

		#endif

			if (Application.isMobilePlatform && interstitial.IsLoaded()) 
			{
				interstitial.Show();
			}
			else
			{
				RequestInterstitial();	
			}

		#if UNITY_IOS

		}

		#endif

		#endif

		#if STAN_ASSET_GOOGLEMOBILEADS

		#if UNITY_IOS

		if (useAdmob)
		{

		#endif

			GoogleMobileAd.OnInterstitialLoaded -= HandleOnInterstitialLoadedGameOver;
			GoogleMobileAd.OnInterstitialFailedLoading -= HandleOnInterstitialFailedLoadingGameOver;
			GoogleMobileAd.OnInterstitialLoaded += HandleOnInterstitialLoadedGameOver;
			GoogleMobileAd.OnInterstitialFailedLoading += HandleOnInterstitialFailedLoadingGameOver;
			GoogleMobileAd.LoadInterstitialAd();

		#if UNITY_IOS

		}

		#endif

		#endif

		#if STAN_ASSET_ANDROIDNATIVE

		#if UNITY_IOS

		if (useAdmob)
		{

		#endif

			AndroidAdMobController.Instance.OnInterstitialLoaded -= HandleOnInterstitialLoadedGameOver;
			AndroidAdMobController.Instance.OnInterstitialFailedLoading -= HandleOnInterstitialFailedLoadingGameOver;
			AndroidAdMobController.Instance.OnInterstitialLoaded += HandleOnInterstitialLoadedGameOver;
			AndroidAdMobController.Instance.OnInterstitialFailedLoading += HandleOnInterstitialFailedLoadingGameOver;
			AndroidAdMobController.Instance.LoadInterstitialAd();

		#if UNITY_IOS

		}

		#endif

		#endif

	}

	private void HandleOnInterstitialLoadedGameOver() 
	{
		#if STAN_ASSET_GOOGLEMOBILEADS

		GoogleMobileAd.OnInterstitialLoaded -= HandleOnInterstitialLoadedGameOver;
		GoogleMobileAd.ShowInterstitialAd();

		#endif

		#if STAN_ASSET_ANDROIDNATIVE

		AndroidAdMobController.Instance.OnInterstitialLoaded -= HandleOnInterstitialLoadedGameOver;
		AndroidAdMobController.Instance.ShowInterstitialAd();

		#endif

	}

	private void HandleOnInterstitialFailedLoadingGameOver()
	{

		#if STAN_ASSET_GOOGLEMOBILEADS

		GoogleMobileAd.OnInterstitialFailedLoading -= HandleOnInterstitialFailedLoadingGameOver;

		#endif

		#if STAN_ASSET_ANDROIDNATIVE

		AndroidAdMobController.Instance.OnInterstitialFailedLoading -= HandleOnInterstitialFailedLoadingGameOver;

		#endif

		#if CHARTBOOST

		Chartboost.showInterstitial(CBLocation.Default);

		#endif

	}


	private void ShowAdmobBackup()
	{
		#if STAN_ASSET_GOOGLEMOBILEADS

		GoogleMobileAd.OnInterstitialLoaded -= HandleOnInterstitialLoadedGameOverBackUp;
		GoogleMobileAd.LoadInterstitialAd();

		#endif

		#if STAN_ASSET_ANDROIDNATIVE

		AndroidAdMobController.Instance.OnInterstitialLoaded -= HandleOnInterstitialLoadedGameOverBackUp;
		AndroidAdMobController.Instance.LoadInterstitialAd();

		#endif

	}

	private void HandleOnInterstitialLoadedGameOverBackUp()
	{
		#if STAN_ASSET_GOOGLEMOBILEADS

		GoogleMobileAd.OnInterstitialLoaded -= HandleOnInterstitialLoadedGameOverBackUp;
		GoogleMobileAd.ShowInterstitialAd();

		#endif

		#if STAN_ASSET_ANDROIDNATIVE

		AndroidAdMobController.Instance.OnInterstitialLoaded -= HandleOnInterstitialLoadedGameOverBackUp;
		AndroidAdMobController.Instance.ShowInterstitialAd();

		#endif

	}


	#if CHARTBOOST

	private void ShowChartboostInterstitialGameOver()
	{
	    Chartboost.didFailToLoadInterstitial -= didFailedToLoaChartboostGameOver;
	    Chartboost.didFailToLoadInterstitial += didFailedToLoaChartboostGameOver;
	    Chartboost.showInterstitial (CBLocation.Default);
	}

	private void didFailedToLoaChartboostGameOver(CBLocation location, CBImpressionError error)
	{
	    Chartboost.didFailToLoadInterstitial -= didFailedToLoaChartboostGameOver;

	    if (location != CBLocation.Startup)
        {
	        Debug.Log(string.Format("--------- didFailedToLoaChartboostGameOver didFailToLoadInterstitial: {0} at location {1}", error, location));
	    } 
        else
        {
	        Debug.Log(string.Format("---------!!!! ERROR !!!!  didFailedToLoaChartboostGameOver didFailToLoadInterstitial: {0} at location {1}", error, location));
	    }
	}

	private void didFailToLoadInterstitialGameOver(CBLocation location, CBImpressionError error)
    {
	    Debug.Log(string.Format("CHARTBOOST ----- didFailToLoadInterstitial: {0} at location {1}", error, location));

	    if (location == CBLocation.Startup)
        {
	        Debug.Log("it's startup ==> on base");
	    } 
        else
        {
	        Debug.Log("it's NOT startup ==> on tente une autre ad");

	        ShowAdmobBackup ();
	    }
	}

	#endif

	private void OnDestroy() 
	{
		HideBanner();
	}
}



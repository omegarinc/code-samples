﻿using UnityEngine;
using System.Collections;

namespace Twist
{
    public class ADCVideoZoneCustom
    {
        #region Public resources
        public string zoneId = "";
        public ADCVideoZoneTypeCustom zoneType = ADCVideoZoneTypeCustom.None;
        #endregion

        #region Public methods
        public ADCVideoZoneCustom(string newZoneId, ADCVideoZoneTypeCustom newVideoZoneType)
        {
            zoneId = newZoneId;
            zoneType = newVideoZoneType;
        }
        #endregion
    }

    public enum ADCVideoZoneTypeCustom
    {
        None,
        Interstitial,
        V4VC
    }
}

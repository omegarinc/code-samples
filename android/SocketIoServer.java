package com.disney.unison.server;


import android.support.annotation.Nullable;
import android.util.Log;

import com.disney.unison.model.ChatMessage;
import com.disney.unison.model.Color;
import com.disney.unison.model.GuestList;
import com.disney.unison.model.KickMessage;
import com.disney.unison.model.MoviedTime;
import com.disney.unison.model.Room;
import com.disney.unison.model.Server;
import com.disney.unison.model.ServerTicket;
import com.disney.unison.model.StatedTime;
import com.disney.unison.model.Time;
import com.disney.unison.model.TypedTime;
import com.disney.unison.model.Visitor;
import com.disney.unison.utils.JsonUtils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Manager;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.Transport;

/**
 * Created by Anton Knyazev on 30.03.2016.
 */
public class SocketIoServer implements Server {
    private static final String TAG = SocketIoServer.class.getSimpleName();
    private final Socket mSocket;
    private Gson mGson;
    private CopyOnWriteArraySet<OnBroadcastMessageListener> mBroadcastListeners = new CopyOnWriteArraySet<>();
    private CopyOnWriteArraySet<OnDirectMessageListener> mDirectListeners = new CopyOnWriteArraySet<>();
    private Map<BroadcastEvent, Object> mLastBroadcastEvent = new ConcurrentHashMap<>();

    private interface Command {
        String OPEN = "OPEN";
        String CLOSE = "CLOSE";
        String INFO = "INFO";
        String JOIN = "JOIN";
        String BROADCAST = "BROADCAST";
        String MESSAGE = "MESSAGE";
    }

    private interface Event {
        String SESSION_OVER  = "SESSIONOVER";
        String IDENTIFY  = "IDENTIFY";
        String GUEST_LIST = "GUESTLIST";
        String CHAT = "CHAT";
        String SYNC = "SYNC";
        String PLAY = "PLAY";
        String PAUSE = "PAUSE";
        String SEEK = "SEEK";
        String STATE = "STATE";
        String PING = "PING";
        String COLOR = "COLOR";
        String KICK = "KICK";
    }

    private interface Result {
        String OK = "OK";
        String ERROR = "ERROR";
    }

    public SocketIoServer(String serverUrl, String unisonUserId, Gson gson)  {
        mGson = gson;
        try {
            mSocket = IO.socket(serverUrl);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        addHeader("x-unison-user-id", unisonUserId);
        mSocket.on(Command.BROADCAST, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                if (args != null && args.length > 1) {
                    String event = (String) args[0];
                    BroadcastEvent broadcastEvent = null;
                    Class parsingClass = null;
                    switch (event) {
                        case Event.SESSION_OVER:
                            onBroadcastMessage(BroadcastEvent.SESSION_OVER, null, null);
                            onClose();
                            break;
                        case Event.GUEST_LIST:
                            broadcastEvent = BroadcastEvent.GUEST_LIST;
                            parsingClass = GuestList.class;
                            break;
                        case Event.IDENTIFY:
                            broadcastEvent = BroadcastEvent.IDENTIFY;
                            parsingClass = Visitor.class;
                            break;
                        case Event.CHAT:
                            broadcastEvent = BroadcastEvent.CHAT;
                            parsingClass = ChatMessage.class;
                            break;
                        case Event.STATE:
                            broadcastEvent = BroadcastEvent.STATE;
                            parsingClass = StatedTime.class;
                            break;
                        case Event.PLAY:
                            broadcastEvent = BroadcastEvent.PLAY;
                            parsingClass = Time.class;
                            break;
                        case Event.PAUSE:
                            broadcastEvent = BroadcastEvent.PAUSE;
                            parsingClass = Time.class;
                            break;
                        case Event.SEEK:
                            broadcastEvent = BroadcastEvent.SEEK;
                            parsingClass = TypedTime.class;
                             break;
                        case Event.SYNC:
                            broadcastEvent = BroadcastEvent.SYNC;
                            parsingClass = MoviedTime.class;
                            break;

                    }
                    if (broadcastEvent != null && args.length > 2 && args[2] != null){
                        Object result =  parse((JSONObject)args[2], parsingClass);
                        mLastBroadcastEvent.put(broadcastEvent, result);
                        Log.v("test", event + " " + result.toString() + " userId=" + args[1]);

                        onBroadcastMessage(broadcastEvent, (String) args[1], parse((JSONObject)args[2], parsingClass));
                    } else {
                        Log.v("test", event);
                    }
                }
            }
        });
        mSocket.on(Command.MESSAGE, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                if (args != null && args.length > 1) {
                    String event = (String) args[0];
                    switch (event) {
                        case Event.IDENTIFY:
                            if (args.length > 2 && args[2] != null) {
                                onDirectMessage(MessageType.IDENTIFY, (String) args[1], parse((JSONObject) args[2], Visitor.class));
                            }
                            break;
                        case Event.COLOR:
                            if (args.length > 2 && args[2] != null) {
                                onDirectMessage(MessageType.COLOR, (String) args[1], parse((JSONObject) args[2], Color.class));
                            }
                            break;
                        case Event.KICK:
                            if (args.length > 2 && args[2] != null) {
                                onDirectMessage(MessageType.KICK, (String) args[1], parse((JSONObject) args[2], KickMessage.class));
                            }
                            break;
                    }
                }
            }
        });
    }

    private void onClose() {
        mLastBroadcastEvent.clear();
    }

    private void onDirectMessage(MessageType messageType, String senderUserId, Object object) {
        for (OnDirectMessageListener listener : mDirectListeners) {
            listener.onDirectServerMessage(messageType, senderUserId, object);
        }
    }

    private void onBroadcastMessage(BroadcastEvent event, String userId, Object object) {
        for (OnBroadcastMessageListener listener : mBroadcastListeners) {
            listener.onBroadcastServerMessage(event, userId, object);
        }
    }


    private void addHeader(final String key, final String... values) {
        mSocket.io().on(Manager.EVENT_TRANSPORT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Transport transport = (Transport) args[0];
                transport.on(Transport.EVENT_REQUEST_HEADERS, new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        @SuppressWarnings("unchecked")
                        Map<String, List<String>> headers = (Map<String, List<String>>)args[0];
                        headers.put(key, Arrays.asList(values));
                    }
                });
            }
        });
    }

    private <T> void request(final String command, @Nullable final Callback<T> callback, @Nullable final Class<T> parsingClass, final Object... arguments) {
        final Emitter.Listener errorListener = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                if (callback != null) {
                    callback.onResponseServer(false, null);
                }

            }
        };

        final Ack ack = new Ack() {
            @Override
            public void call(Object... args) {
                mSocket.off(Socket.EVENT_ERROR, errorListener);
                if (callback == null) {
                    return;
                }
                String result = (String) args[0];
                switch (result) {
                    case Result.OK:
                        if (parsingClass != null) {
                            JSONObject obj = (JSONObject) args[1];
                            callback.onResponseServer(true, parse(obj, parsingClass));
                        } else {
                            callback.onResponseServer(true, null);
                        }
                        break;
                    case Result.ERROR:
                        if (args.length >= 2) {
                            Log.e(TAG, "Error: " + args[1]);
                        }
                        callback.onResponseServer(false, null);
                        break;

                }

            }
        };

        mSocket.once(Socket.EVENT_ERROR, errorListener);
        if (mSocket.connected()) {
            mSocket.emit(command, arguments, ack);
        } else {
            mSocket.once(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    mSocket.emit(command, arguments, ack);
                }
            });
            mSocket.connect();
        }
    }

    private <T> T parse(String json, Class<T> cls) {
        return mGson.fromJson(json, cls);
    }


    private <T> T parse(JSONObject object, Class<T> cls) {
        return parse(object.toString(), cls);
    }

    @Override
    public void createRoom(final String roomName, final String movieId, final Callback<Room> callback) {
        Callback<Room> callbackWrapper = new Callback<Room>() {
            @Override
            public void onResponseServer(boolean success, Room room) {
                if (!success) {
                    closeRoom(null);
                }
                callback.onResponseServer(success, room);
            }
        };
        request(Command.OPEN, callbackWrapper, Room.class, roomName, JsonUtils.create("movieId", movieId));
    }

    @Override
    public void infoRoom(final String roomName, final Callback<Room> callback) {
        request(Command.INFO, callback, Room.class, roomName);
    }

    @Override
    public void joinRoom(final String roomName, final Callback<ServerTicket> callback) {
        Callback<ServerTicket> callbackWrapper = new Callback<ServerTicket>() {
            @Override
            public void onResponseServer(boolean success, ServerTicket serverTicket) {
                if (!success) {
                    closeRoom(null);
                }
                callback.onResponseServer(success, serverTicket);
            }
        };
        request(Command.JOIN, callbackWrapper, ServerTicket.class, roomName);
    }

    @Override
    public void closeRoom(@Nullable final Callback<Object> callback) {
        request(Command.CLOSE, callback, null);
    }

    @Override
    public void connect() {
        mSocket.connect();
    }

    @Override
    public void disconnect() {
        onClose();
        mSocket.disconnect();
    }

    @Override
    public void sendMessage(MessageType messageType, String userId, Object object) {
        String messageTypeString;
        switch (messageType) {
            case PING:
                messageTypeString = Event.PING;
                break;
            case IDENTIFY:
                messageTypeString = Event.IDENTIFY;
                break;
            case COLOR:
                messageTypeString = Event.COLOR;
                break;
            case KICK:
                messageTypeString = Event.KICK;
                break;
            default:
                throw new UnsupportedOperationException();
        }
        try {
            mSocket.emit(Command.MESSAGE, messageTypeString, userId, new JSONObject(mGson.toJson(object)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String getServerEvent(BroadcastEvent event) {
        String eventServer;
        switch (event) {
            case IDENTIFY:
                eventServer = Event.IDENTIFY;
                break;
            case GUEST_LIST:
                eventServer = Event.GUEST_LIST;
                break;
            case CHAT:
                eventServer = Event.CHAT;
                break;
            case STATE:
                eventServer = Event.STATE;
                break;
            case PLAY:
                eventServer = Event.PLAY;
                break;
            case PAUSE:
                eventServer = Event.PAUSE;
                break;
            case SEEK:
                eventServer = Event.SEEK;
                break;
            case SYNC:
                eventServer = Event.SYNC;
                break;
            default:
                throw new UnsupportedOperationException();
        }
        return eventServer;
    }


    @Override
    public void sendBroadcastMessage(BroadcastEvent event, Object object) {
        try {
            mSocket.emit(Command.BROADCAST, getServerEvent(event), new JSONObject(mGson.toJson(object)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendBroadcastMessage(BroadcastEvent event, Object object, final @Nullable OnResultSendListener listener) {
        try {
            mSocket.emit(Command.BROADCAST, getServerEvent(event), new JSONObject(mGson.toJson(object)), new Ack() {
                @Override
                public void call(Object... args) {
                    if (listener != null) {
                        listener.onResultSend(true);
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addBroadcastMessageListener(OnBroadcastMessageListener listener) {
        mBroadcastListeners.add(listener);
    }

    @Override
    public void removeBroadcastMessageListener(OnBroadcastMessageListener listener) {
        mBroadcastListeners.remove(listener);
    }

    @Override
    public void addDirectMessageListener(OnDirectMessageListener listener) {
        mDirectListeners.add(listener);
    }

    @Override
    public void removeDirectMessageListener(OnDirectMessageListener listener) {
        mDirectListeners.remove(listener);
    }

    @Override
    public Object getLastBroadcastEvent(BroadcastEvent event) {
        return mLastBroadcastEvent.get(event);
    }

}

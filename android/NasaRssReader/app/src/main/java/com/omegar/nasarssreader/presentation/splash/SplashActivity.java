package com.omegar.nasarssreader.presentation.splash;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.omegar.nasarssreader.R;
import com.omegar.nasarssreader.presentation.item_list.ItemListActivity;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
public class SplashActivity extends AppCompatActivity {

    private static final int MIN_TIME = 2500; // in msec

    private long mStartTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mStartTime = System.currentTimeMillis();

        startActivityDelayed(new Intent(this, ItemListActivity.class));
    }

    private void startActivityDelayed(final Intent intent) {
        long delay = MIN_TIME - (System.currentTimeMillis() - mStartTime);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(intent);
                finish();
            }
        }, delay);
    }
}

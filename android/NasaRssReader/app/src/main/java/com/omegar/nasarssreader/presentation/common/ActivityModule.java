package com.omegar.nasarssreader.presentation.common;

import com.omegar.nasarssreader.presentation.di.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
@Module()
public class ActivityModule {

    private BaseActivity mActivity;

    public ActivityModule(BaseActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @PerActivity
    BaseActivity activity() {
        return this.mActivity;
    }

}

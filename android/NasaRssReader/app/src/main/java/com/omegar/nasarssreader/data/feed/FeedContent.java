package com.omegar.nasarssreader.data.feed;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
public class FeedContent implements Serializable {

    public static final String FEED_STATUS_OK = "ok";

    private String status;
    private FeedInfo feed;
    private List<FeedItem> items = new ArrayList<>();


    // check if Feed status is "OK"
    public boolean isStatusValid() {
        return !TextUtils.isEmpty(status) && status.equalsIgnoreCase(FEED_STATUS_OK);
    }

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The feed
     */
    public FeedInfo getFeedInfo() {
        return feed;
    }

    /**
     *
     * @param feed
     * The feed
     */
    public void setFeedInfo(FeedInfo feed) {
        this.feed = feed;
    }

    /**
     *
     * @return
     * The items
     */
    public List<FeedItem> getItems() {
        return items;
    }

    /**
     *
     * @param items
     * The items
     */
    public void setItems(List<FeedItem> items) {
        this.items = items;
    }
}

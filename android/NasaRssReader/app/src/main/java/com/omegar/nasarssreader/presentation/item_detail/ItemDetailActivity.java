package com.omegar.nasarssreader.presentation.item_detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.omegar.nasarssreader.R;
import com.omegar.nasarssreader.data.feed.FeedItem;
import com.omegar.nasarssreader.presentation.common.BaseActivity;
import com.omegar.nasarssreader.presentation.common.BaseFragment;
import com.omegar.nasarssreader.presentation.common.BasePresenter;
import com.omegar.nasarssreader.presentation.di.HasComponent;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
public class ItemDetailActivity extends BaseActivity implements HasComponent<ItemDetailActivityComponent> {
    private final static String TAG = ItemDetailActivity.class.getSimpleName();
    private static final String EXTRA_ITEM = "EXTRA_ITEM";

    ItemDetailActivityComponent mComponent;

    public static Intent getCallingIntent(Context context, FeedItem item) {
        Intent intent = new Intent(context, ItemDetailActivity.class);
        intent.putExtra(EXTRA_ITEM, item);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initToolbar();

        FeedItem item = (FeedItem)getIntent().getExtras().get(EXTRA_ITEM);
        if (item != null) {
            ItemDetailFragment fragment = ItemDetailFragment.newInstance(item);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_feeditem_detail, fragment)
                    .commit();
            addBackStack(fragment);

            setTitle(item.getTitle());
        }
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_item_detail;
    }

    @Override
    public void initDiComponent() {
        mComponent = DaggerItemDetailActivityComponent
                .builder()
                .appComponent(getAppComponent())
                .activityModule(getActivityModule())
                .build();
    }

    @Override
    protected BasePresenter getPresenter() {
        return null;
    }

    private void addBackStack(BaseFragment fragment) {
        FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
        tx.replace(R.id.container_feeditem_detail, fragment);
        //tx.addToBackStack(fragment.getFragmentName());
        tx.commit();
    }

    @Override
    public ItemDetailActivityComponent getComponent() {
        return mComponent;
    }
}

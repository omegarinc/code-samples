package com.omegar.nasarssreader.presentation.item_detail;

import com.omegar.nasarssreader.presentation.common.ActivityModule;
import com.omegar.nasarssreader.presentation.di.ActivityComponent;
import com.omegar.nasarssreader.presentation.di.AppComponent;
import com.omegar.nasarssreader.presentation.di.scope.PerActivity;
import com.omegar.nasarssreader.presentation.di.scope.PerApplication;

import dagger.Component;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
@PerActivity
@Component(dependencies = AppComponent.class, modules = {ActivityModule.class})
public interface ItemDetailActivityComponent extends ActivityComponent {

    void inject(ItemDetailFragment detailsFragment);
}

package com.omegar.nasarssreader.presentation.item_detail;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.omegar.nasarssreader.R;
import com.omegar.nasarssreader.data.feed.FeedItem;
import com.omegar.nasarssreader.presentation.common.BaseFragment;
import com.omegar.nasarssreader.presentation.common.BasePresenter;
import com.omegar.nasarssreader.presentation.item_list.ItemListActivityComponent;

import javax.inject.Inject;

import butterknife.Bind;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
public class ItemDetailFragment extends BaseFragment implements ItemDetailView {

    private static final String EXTRA_FRAGMENT_ITEM = "EXTRA_FRAGMENT_ITEM";

    @Inject
    ItemDetailPresenter mPresenter;

    @Bind(R.id.image_item) ImageView mItemImageView;
    @Bind(R.id.label_title) TextView mTitleTextView;
    @Bind(R.id.label_date) TextView mDateTextView;
    @Bind(R.id.label_author) TextView mAuthorTextView;
    @Bind(R.id.label_description) TextView mDescriptionTextView;
    @Bind(R.id.label_link) TextView mLinkTextView;

    public static ItemDetailFragment newInstance(FeedItem item) {
        ItemDetailFragment fragment = new ItemDetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_FRAGMENT_ITEM, item);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    protected int getContentViewId() {
        return R.layout.fragment_item_detail;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FeedItem item = (FeedItem) getArguments().getSerializable(EXTRA_FRAGMENT_ITEM);
        assert item != null;

        mPresenter.loadItem(item);
    }

    @NonNull
    @Override
    protected BasePresenter getPresenter() {
        return mPresenter;
    }

    @Override
    protected void inject() {
        if (getActivity() != null) {
            if (getActivity() instanceof ItemDetailActivity) {
                this.getComponent(ItemDetailActivityComponent.class).inject(this);
            } else {
                this.getComponent(ItemListActivityComponent.class).inject(this);
            }
        }
    }

    @Override
    public void displayFeedItem(FeedItem item) {
        if (item.getEnclosure() != null
                && !TextUtils.isEmpty(item.getEnclosure().getLink())) {
            Glide.with(getContext()).load(item.getEnclosure().getLink()).asBitmap().fitCenter().into(mItemImageView);
        } else {
            mItemImageView.setImageDrawable(null);
        }

        mTitleTextView.setText(item.getTitle());

        mAuthorTextView.setText(!TextUtils.isEmpty(item.getAuthor())
                ? String.format(getContext().getString(R.string.label_by_author), item.getAuthor())
                : getContext().getString(R.string.label_by_author_unknown));


        mDateTextView.setText(item.getPubDate());
        mDescriptionTextView.setText(item.getDescription());

        mLinkTextView.setText(Html.fromHtml(String.format(getString(R.string.label_article_link), item.getLink())));
        mLinkTextView.setMovementMethod(LinkMovementMethod.getInstance());
    }

}

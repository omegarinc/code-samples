package com.omegar.nasarssreader.presentation.common;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.omegar.nasarssreader.presentation.di.HasComponent;

import java.util.concurrent.atomic.AtomicInteger;

import butterknife.ButterKnife;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
public abstract class BaseFragment extends Fragment {
    private static final AtomicInteger sLastFragmentId = new AtomicInteger(0);
    private final int mFragmentId;

    abstract protected int getContentViewId();

    public BaseFragment() {
        mFragmentId = sLastFragmentId.incrementAndGet();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getContentViewId(), null);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        inject();

        //noinspection unchecked
        getPresenter().setView(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        getPresenter().onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        getPresenter().onStop();
    }


    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    public String getFragmentName() {
        return Long.toString(mFragmentId);
    }

    @NonNull
    protected abstract BasePresenter getPresenter();

    @SuppressWarnings("unchecked")
    protected <C> C getComponent(Class<C> componentType) {
        return componentType.cast(((HasComponent<C>) getActivity()).getComponent());
    }

    protected abstract void inject();

    // common stuff for fragments

    public void showLoading(String message) {

    }

    public void showLoading() {

    }

    public void hideLoading() {

    }

    public void showError(@StringRes int messageResId) {

    }

    public void showError(String message) {

    }

    public void finishView() {

    }

    public void setTitle(String title) {

    }

}

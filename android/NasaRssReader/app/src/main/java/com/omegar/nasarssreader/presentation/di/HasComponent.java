package com.omegar.nasarssreader.presentation.di;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
public interface HasComponent<T> {
    T getComponent();
    void initDiComponent();
}

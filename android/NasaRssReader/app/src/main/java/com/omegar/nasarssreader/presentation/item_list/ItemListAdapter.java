package com.omegar.nasarssreader.presentation.item_list;

import android.content.Context;
import android.support.annotation.ColorInt;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.omegar.nasarssreader.R;
import com.omegar.nasarssreader.data.feed.FeedItem;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
public class ItemListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<FeedItem> mItems;
    private FeedItem mSelectedItem;

    // define listener member variable
    private static OnItemClickListener mListener;

    public ItemListAdapter(Context context, List<FeedItem> items) {
        this.mContext = context;
        this.mItems = items;
    }

    // define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    // define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mListener = listener;
    }

    // return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return this.mItems.size();
    }

    public void addAll(List<FeedItem> items){
        mItems.addAll(items);
        notifyDataSetChanged();
    }

    public void clear(){
        mItems.clear();
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        View v = inflater.inflate(R.layout.row_feed_item, viewGroup, false);
        viewHolder = new ItemViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ItemViewHolder vh = (ItemViewHolder) viewHolder;
        configureSimpleViewHolder(vh, position);
    }

    private void configureSimpleViewHolder(final ItemViewHolder vh, int position) {
        FeedItem item = mItems.get(position);

        boolean isItemSelected = mSelectedItem != null
                && item.getGuid().equalsIgnoreCase(mSelectedItem.getGuid());

        int bgndColor = isItemSelected
                ? ContextCompat.getColor(mContext, R.color.colorAccent)
                : ContextCompat.getColor(mContext, R.color.color_bgnd_default);
        int textColor = isItemSelected
                ? ContextCompat.getColor(mContext, R.color.color_text_light)
                : ContextCompat.getColor(mContext, R.color.color_text);

        vh.viewContent.setBackgroundColor(getColorFixed(bgndColor));

        vh.titleTextView.setTextColor(getColorFixed(textColor));
        vh.authorTextView.setTextColor(getColorFixed(textColor));

        if (item.getEnclosure() != null
                && !TextUtils.isEmpty(item.getEnclosure().getLink())) {
            Glide.with(mContext).load(item.getEnclosure().getLink()).asBitmap().fitCenter().into(vh.itemImage);
        } else {
            vh.itemImage.setImageDrawable(null);
        }

        vh.titleTextView.setText(item.getTitle());

        vh.authorTextView.setText(!TextUtils.isEmpty(item.getAuthor())
                ? String.format(mContext.getString(R.string.label_by_author), item.getAuthor())
                : mContext.getString(R.string.label_by_author_unknown));

        ViewTreeObserver observer = vh.titleTextView.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int maxLines = (int) vh.titleTextView.getHeight()
                        / vh.titleTextView.getLineHeight();
                vh.titleTextView.setMaxLines(maxLines);
                vh.titleTextView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    public @ColorInt int getColorFixed(int color) {
        return color;
    }

    public static class BaseViewHolder extends RecyclerView.ViewHolder {
        public BaseViewHolder(final View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // triggers click upwards to the adapter on click
                    if (mListener != null) {
                        mListener.onItemClick(itemView, getLayoutPosition());
                    }
                }
            });
        }
    }

    public static class ItemViewHolder extends BaseViewHolder {
        @Bind(R.id.view_content) public LinearLayout viewContent;
        @Bind(R.id.image_item) public ImageView itemImage;
        @Bind(R.id.label_title) public TextView titleTextView;
        @Bind(R.id.label_author) public TextView authorTextView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setSelectedItem(FeedItem selectedItem) {
        this.mSelectedItem = selectedItem;
    }
}

package com.omegar.nasarssreader.presentation.di;

import android.content.Context;

import com.omegar.nasarssreader.App;
import com.omegar.nasarssreader.domain.feed.FeedDataProvider;
import com.omegar.nasarssreader.presentation.common.BaseActivity;
import com.omegar.nasarssreader.presentation.di.scope.PerApplication;

import javax.inject.Named;

import dagger.Component;
import rx.Scheduler;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
@PerApplication
@Component(modules = {AppModule.class})
public interface AppComponent {

    void inject(App app);
    void inject(BaseActivity baseActivity);

    Context context();

    @Named(AppModule.JOB) Scheduler jobScheduler();
    @Named(AppModule.UI) Scheduler uIScheduler();
    FeedDataProvider feedDataProvider();

}

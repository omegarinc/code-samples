package com.omegar.nasarssreader.presentation.common;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.omegar.nasarssreader.App;
import com.omegar.nasarssreader.R;
import com.omegar.nasarssreader.presentation.di.AppComponent;

import butterknife.ButterKnife;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
public abstract class BaseActivity extends AppCompatActivity {
    private static final String TAG = BaseActivity.class.getSimpleName();

    private ProgressDialog mProgressDialog;
    private int mCountShowProgressDialog;

    protected Toolbar mToolbar;

    abstract protected int getContentViewId();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        initDiComponent();
        super.onCreate(savedInstanceState);

        setContentView(getContentViewId());
        ButterKnife.bind(this);

        if (getPresenter() != null) {
            //noinspection unchecked
            getPresenter().setView(this);
        }
    }

    public void initDiComponent() {

    }

    protected AppComponent getAppComponent() {
        return ((App) getApplication()).getComponent();
    }

    protected ActivityModule getActivityModule() {
        return new ActivityModule(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    protected abstract BasePresenter getPresenter();

    @Override
    public void onStart() {
        super.onStart();

        if (getPresenter() != null) {
            getPresenter().onStart();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (getPresenter() != null) {
            getPresenter().onStop();
        }
    }

    // common stuff below

    @Override
    public void setSupportActionBar(Toolbar toolbar) {
        super.setSupportActionBar(toolbar);
        initActionBar();
    }

    private void initActionBar() {
        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayShowHomeEnabled(true);
            bar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_TITLE);
        }
    }

    protected void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }
    }

    private void createProgressDialog() {
        try {
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.setCanceledOnTouchOutside(false);
            }
        } catch (Exception e) {
            Log.e(TAG, "error", e);
        }
    }

    public void hideLoading() {
        try {
            mCountShowProgressDialog--;
            if (mCountShowProgressDialog <= 0) {
                if (!isFinishing()) {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                }
                mCountShowProgressDialog = 0;
            }
        } catch (Exception e) {
            Log.e(TAG, "error", e);
        }
    }

    public void showLoading(String message) {
        try {
            createProgressDialog();
            mProgressDialog.setMessage(message == null ? getString(R.string.loading) : message);
            mProgressDialog.show();
            mCountShowProgressDialog++;
        } catch (Exception e) {
            Log.e(TAG, "error", e);
        }
    }

    public void showLoading() {
        showLoading(null);
    }

    public boolean isShowProgressDialog() {
        return mCountShowProgressDialog > 0;
    }

    public void showProgressDialog() {
        showLoading(null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showSnackbar(@StringRes int msgResId) {
        View parentLayout = findViewById(android.R.id.content);
        if (parentLayout != null) {
            Snackbar.make(parentLayout, msgResId, Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    public void showSnackbar(String message) {
        View parentLayout = findViewById(android.R.id.content);
        if (parentLayout != null) {
            Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    public void showError(@StringRes int messageResId) {
        showSnackbar(messageResId);
    }

    public void showError(String message) {
        showSnackbar(message);
    }

    public void finishView() {
        finish();
    }

    public void setTitle(String title) {
        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setTitle(title);
        }
    }

}


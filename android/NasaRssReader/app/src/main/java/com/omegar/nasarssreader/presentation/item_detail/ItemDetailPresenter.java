package com.omegar.nasarssreader.presentation.item_detail;

import com.omegar.nasarssreader.data.feed.FeedItem;
import com.omegar.nasarssreader.presentation.common.BasePresenter;
import com.omegar.nasarssreader.presentation.di.scope.PerActivity;
import com.omegar.nasarssreader.presentation.navigation.ItemDetailNavigator;

import javax.inject.Inject;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
@PerActivity
public class ItemDetailPresenter extends BasePresenter<ItemDetailView, ItemDetailNavigator> {
    private final static String TAG = ItemDetailPresenter.class.getSimpleName();

    private FeedItem mItem;

    @Inject
    public ItemDetailPresenter() {
    }

    @Override
    public void onStart() {

    }

    public void loadItem(FeedItem item) {
        // just forward item, no info retrieving
        mItem = item;
        getView().displayFeedItem(mItem);
    }

    @Override
    public void onStop() {

    }
}



package com.omegar.nasarssreader.presentation.item_list;

import com.omegar.nasarssreader.presentation.common.ActivityModule;
import com.omegar.nasarssreader.presentation.di.ActivityComponent;
import com.omegar.nasarssreader.presentation.di.AppComponent;
import com.omegar.nasarssreader.presentation.di.scope.PerActivity;
import com.omegar.nasarssreader.presentation.item_detail.ItemDetailFragment;

import dagger.Component;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
@PerActivity
@Component(dependencies = AppComponent.class, modules = {ActivityModule.class})
public interface ItemListActivityComponent extends ActivityComponent {

    void inject(ItemListActivity activity);
    void inject(ItemDetailFragment detailFragment);
}

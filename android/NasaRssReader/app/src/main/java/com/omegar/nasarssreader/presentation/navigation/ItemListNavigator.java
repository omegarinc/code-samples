package com.omegar.nasarssreader.presentation.navigation;

import com.omegar.nasarssreader.data.feed.FeedItem;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
public interface ItemListNavigator {
    void showFeedItemDetail(FeedItem item);
}

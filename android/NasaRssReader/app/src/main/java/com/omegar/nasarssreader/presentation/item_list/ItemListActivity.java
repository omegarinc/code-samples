package com.omegar.nasarssreader.presentation.item_list;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.omegar.nasarssreader.R;
import com.omegar.nasarssreader.data.feed.FeedContent;
import com.omegar.nasarssreader.data.feed.FeedItem;
import com.omegar.nasarssreader.presentation.common.BaseActivity;
import com.omegar.nasarssreader.presentation.common.BasePresenter;
import com.omegar.nasarssreader.presentation.di.HasComponent;
import com.omegar.nasarssreader.presentation.item_detail.ItemDetailActivity;
import com.omegar.nasarssreader.presentation.item_detail.ItemDetailFragment;
import com.omegar.nasarssreader.presentation.navigation.ItemListNavigator;
import com.omegar.nasarssreader.presentation.view.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
public class ItemListActivity extends BaseActivity implements HasComponent<ItemListActivityComponent>,
        ItemListNavigator, ItemListView, ItemListAdapter.OnItemClickListener {

    private final static String TAG = ItemListActivity.class.getSimpleName();

    @Bind(R.id.swipe_refresh_items)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @Bind(R.id.list_feeds)
    RecyclerView mFeedList;

    private ItemListAdapter mItemListAdapter;

    ItemListActivityComponent mComponent;

    @Inject
    ItemListPresenter mPresenter;

    private boolean mTwoPane;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_item_list;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //noinspection unchecked
        getPresenter().setNavigator(this);

        initToolbar();

        if (findViewById(R.id.container_feeditem_detail) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        mPresenter.loadFeed();
                    }
                }
        );

        // set empty adapter
        mItemListAdapter = new ItemListAdapter(this, new ArrayList<FeedItem>());
        mItemListAdapter.setOnItemClickListener(this);
        mFeedList.setAdapter(mItemListAdapter);

        // set simple divider
        RecyclerView.ItemDecoration itemDecoration = new
                DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST);
        mFeedList.addItemDecoration(itemDecoration);

        // show progress dialog for the first time, next time only swipe refresh
        showLoading();
        mPresenter.loadFeed();
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void initDiComponent() {
        mComponent = DaggerItemListActivityComponent
                .builder()
                .appComponent(getAppComponent())
                .activityModule(getActivityModule())
                .build();
        mComponent.inject(this);
    }

    @Override
    protected void initToolbar() {
        super.initToolbar();

        setTitle("");

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayShowHomeEnabled(false);
            bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE);
        }
    }

    @NonNull
    @Override
    protected BasePresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public ItemListActivityComponent getComponent() {
        return mComponent;
    }

    @Override
    public void showFeedItemDetail(FeedItem item) {
        if (mTwoPane) {
            mItemListAdapter.setSelectedItem(item);
            mItemListAdapter.notifyDataSetChanged();

            // reset detail fragment scroll position
            NestedScrollView scrollView = (NestedScrollView) findViewById(R.id.container_feeditem_detail);
            if (scrollView != null) {
                scrollView.scrollTo(0, 0);
            }

            // show in detail fragment
            ItemDetailFragment fragment = ItemDetailFragment.newInstance(item);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_feeditem_detail, fragment)
                    .commit();
        } else {
            // show in detail activity
            startActivity(ItemDetailActivity.getCallingIntent(this, item));
        }
    }

    @Override
    public void displayFeed(List<FeedItem> items) {
        mItemListAdapter.clear();
        mItemListAdapter.addAll(items);
    }

    @Override
    public void selectItem(FeedItem item) {
        if (mTwoPane) {
            showFeedItemDetail(item);
        }
    }

    @Override
    public void onItemClick(View itemView, int position) {
        mPresenter.itemSelected(position);
    }
}

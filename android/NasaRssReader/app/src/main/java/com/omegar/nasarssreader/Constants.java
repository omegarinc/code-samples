package com.omegar.nasarssreader;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
public class Constants {

    // rss2json Retrofit
    public static final String RSS_FEED_CONVERTER_BASE_URL = "http://rss2json.com/";

    // NASA RSS feed url
    public static final String NASA_RSS_FEED_URL = "https://www.nasa.gov/rss/dyn/breaking_news.rss";

}

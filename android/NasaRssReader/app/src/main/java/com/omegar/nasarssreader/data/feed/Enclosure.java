package com.omegar.nasarssreader.data.feed;

import java.io.Serializable;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
public class Enclosure implements Serializable {

    private String link;
    private String type;
    private Long length;

    /**
     *
     * @return
     * The link
     */
    public String getLink() {
        return link;
    }

    /**
     *
     * @param link
     * The link
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     *
     * @return
     * The type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The length
     */
    public Long getLength() {
        return length;
    }

    /**
     *
     * @param length
     * The length
     */
    public void setLength(Long length) {
        this.length = length;
    }
}

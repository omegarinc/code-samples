package com.omegar.nasarssreader.presentation.di;

import com.omegar.nasarssreader.presentation.common.ActivityModule;
import com.omegar.nasarssreader.presentation.common.BaseActivity;
import com.omegar.nasarssreader.presentation.di.scope.PerActivity;

import dagger.Component;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
@PerActivity
@Component(dependencies = AppComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    // exposed to sub-graphs
    BaseActivity activity();
}

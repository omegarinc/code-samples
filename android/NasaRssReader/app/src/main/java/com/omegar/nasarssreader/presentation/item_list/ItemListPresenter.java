package com.omegar.nasarssreader.presentation.item_list;

import android.text.TextUtils;

import com.omegar.nasarssreader.R;
import com.omegar.nasarssreader.data.feed.FeedContent;
import com.omegar.nasarssreader.data.feed.FeedInfo;
import com.omegar.nasarssreader.data.feed.FeedItem;
import com.omegar.nasarssreader.domain.feed.GetFeedInteractor;
import com.omegar.nasarssreader.presentation.common.BasePresenter;
import com.omegar.nasarssreader.presentation.di.scope.PerActivity;
import com.omegar.nasarssreader.presentation.navigation.ItemListNavigator;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Subscriber;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
@PerActivity
public class ItemListPresenter extends BasePresenter<ItemListView, ItemListNavigator> {
    private final static String TAG = ItemListPresenter.class.getSimpleName();

    private final GetFeedInteractor mFeedInteractor;

    private List<FeedItem> mItems = new ArrayList<>();

    @Inject
    public ItemListPresenter(GetFeedInteractor feedInteractor) {
        this.mFeedInteractor = feedInteractor;
    }

    @Override
    public void onStart() {

    }

    public void loadFeed() {
        mFeedInteractor.execute(new Subscriber<FeedContent>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                getView().hideLoading();
                getView().showError(R.string.error_unknown);

                mItems = new ArrayList<>();
                getView().displayFeed(mItems);
            }

            @Override
            public void onNext(FeedContent feedContent) {
                getView().hideLoading();

                if (feedContent != null && feedContent.isStatusValid()) {
                    FeedInfo fi = feedContent.getFeedInfo();
                    if (fi != null && !TextUtils.isEmpty(fi.getTitle())) {
                        // set feed title
                        getView().setTitle(fi.getTitle());
                    }
                    mItems = feedContent.getItems();
                } else {
                    mItems = new ArrayList<>();
                }
                // display feed items
                getView().displayFeed(mItems);

                if (mItems.size() > 0) {
                    // pre-select first item if any
                    getView().selectItem(mItems.get(0));
                }
            }
        });
    }

    public void itemSelected(int position) {
        getNavigator().showFeedItemDetail(mItems.get(position));
    }

    @Override
    public void onStop() {
        mFeedInteractor.unsubscribe();
    }
}


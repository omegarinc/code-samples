package com.omegar.nasarssreader.data.feed;

import com.omegar.nasarssreader.Constants;
import com.omegar.nasarssreader.domain.feed.FeedDataProvider;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
public class FeedDataProviderImpl implements FeedDataProvider {
    private static final String TAG = FeedDataProviderImpl.class.getSimpleName();

    private FeedApi mFeedApi;

    @Inject
    public FeedDataProviderImpl(FeedApi feedApi) {
        this.mFeedApi = feedApi;
    }

    // get NASA RSS feed
    public Observable<FeedContent> getFeedData() {
        return mFeedApi.getFeedData(Constants.NASA_RSS_FEED_URL);
    }


}

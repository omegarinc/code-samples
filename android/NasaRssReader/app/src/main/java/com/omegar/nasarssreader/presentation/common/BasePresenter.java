package com.omegar.nasarssreader.presentation.common;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
public abstract class BasePresenter<View, Navigator> {
    private View mView;
    private Navigator mNavigator;

    public abstract void onStart();

    public abstract void onStop();

    public View getView() {
        return mView;
    }

    public void setView(View view) {
        this.mView = view;
    }

    public Navigator getNavigator() {
        return mNavigator;
    }

    public void setNavigator(Navigator navigator) {
        this.mNavigator = navigator;
    }
}

package com.omegar.nasarssreader.presentation.di;

import android.content.Context;

import com.omegar.nasarssreader.App;
import com.omegar.nasarssreader.Constants;
import com.omegar.nasarssreader.data.feed.FeedApi;
import com.omegar.nasarssreader.data.feed.FeedDataProviderImpl;
import com.omegar.nasarssreader.domain.feed.FeedDataProvider;
import com.omegar.nasarssreader.presentation.di.scope.PerApplication;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
@Module()
public class AppModule {
    protected final App mApplication;

    public AppModule(App application) {
        this.mApplication = application;

    }

    @Provides
    @PerApplication
    public Context provideContext() {
        return mApplication.getApplicationContext();
    }

    // expose the application to the graph
    @Provides
    @Singleton
    App application() {
        return mApplication;
    }

    // schedulers for working with api
    public static final String JOB = "JOB";
    public static final String UI = "UI";

    @Provides
    @PerApplication
    @Named(JOB)
    public Scheduler provideJobScheduler() {
        return Schedulers.computation();
    }

    @Provides
    @PerApplication
    @Named(UI)
    public Scheduler provideUIScheduler() {
        return AndroidSchedulers.mainThread();
    }

    @Provides
    @PerApplication
    FeedDataProvider provideAuthDataProvider(FeedDataProviderImpl feedDataProvider) {
        return feedDataProvider;
    }

    // feed converter retrofit instance
    private static Retrofit feedDataRetrofit = new Retrofit.Builder()
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(Constants.RSS_FEED_CONVERTER_BASE_URL)
            .build();

    @Provides
    @PerApplication
    FeedApi provideFeedApi() {
        return feedDataRetrofit.create(FeedApi.class);
    }

}

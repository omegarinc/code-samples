package com.omegar.nasarssreader.domain.feed;

import com.omegar.nasarssreader.data.feed.FeedContent;

import java.util.List;

import rx.Observable;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
public interface FeedDataProvider {
    Observable<FeedContent> getFeedData();
}

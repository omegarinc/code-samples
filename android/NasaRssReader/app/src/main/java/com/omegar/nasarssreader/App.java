package com.omegar.nasarssreader;

import android.app.Application;

import com.omegar.nasarssreader.presentation.di.AppComponent;
import com.omegar.nasarssreader.presentation.di.AppModule;
import com.omegar.nasarssreader.presentation.di.DaggerAppComponent;
import com.omegar.nasarssreader.presentation.di.HasComponent;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
public class App extends Application implements HasComponent<AppComponent> {

    private AppComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        initDiComponent();
        mComponent.inject(this);
    }

    @Override
    public AppComponent getComponent() {
        return mComponent;
    }

    @Override
    public void initDiComponent() {
        this.mComponent = createComponent();
    }

    public AppComponent createComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

}


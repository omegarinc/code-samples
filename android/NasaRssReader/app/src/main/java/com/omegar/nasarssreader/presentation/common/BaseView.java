package com.omegar.nasarssreader.presentation.common;

import android.support.annotation.StringRes;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
public interface BaseView {
    void showLoading(String message);
    void showLoading();
    void hideLoading();

    void showError(@StringRes int messageResId);
    void showError(String message);

    void finishView();

    void setTitle(String title);

}

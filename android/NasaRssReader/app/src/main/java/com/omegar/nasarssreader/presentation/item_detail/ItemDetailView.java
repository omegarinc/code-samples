package com.omegar.nasarssreader.presentation.item_detail;

import com.omegar.nasarssreader.data.feed.FeedItem;
import com.omegar.nasarssreader.presentation.common.BaseView;

import java.util.List;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
public interface ItemDetailView extends BaseView {
    void displayFeedItem(FeedItem item);
}

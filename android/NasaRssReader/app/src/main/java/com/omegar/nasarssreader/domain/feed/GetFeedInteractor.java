package com.omegar.nasarssreader.domain.feed;

import com.omegar.nasarssreader.data.feed.FeedContent;
import com.omegar.nasarssreader.domain.common.Interactor;
import com.omegar.nasarssreader.presentation.di.AppModule;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observable;
import rx.Scheduler;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
public class GetFeedInteractor extends Interactor<FeedContent, Void> {

    private final FeedDataProvider mFeedDataProvider;

    @Inject
    public GetFeedInteractor(@Named(AppModule.JOB) Scheduler jobScheduler,
                             @Named(AppModule.UI) Scheduler uiScheduler,
                             FeedDataProvider feedDataProvider) {
        super(jobScheduler, uiScheduler);

        this.mFeedDataProvider = feedDataProvider;
    }

    @Override
    protected Observable<FeedContent> buildObservable(Void parameter) {
        return mFeedDataProvider.getFeedData();
    }

}

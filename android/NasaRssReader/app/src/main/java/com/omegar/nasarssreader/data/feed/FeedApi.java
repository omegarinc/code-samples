package com.omegar.nasarssreader.data.feed;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
public interface FeedApi {

    @GET("api.json")
    Observable<FeedContent> getFeedData(@Query("rss_url") String feedUrl);
}

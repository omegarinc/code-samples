package com.omegar.nasarssreader.domain.common;

import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Sergey Yakimov on 16.05.2016.
 *
 * Copyright 2016 Omega-R, Inc.
 */
public abstract class Interactor<ResultType, ParameterType> {
    private final CompositeSubscription mSubscription = new CompositeSubscription();

    protected final Scheduler mJobScheduler;
    private final Scheduler mUiScheduler;

    public Interactor(Scheduler jobScheduler, Scheduler uiScheduler) {
        this.mJobScheduler = jobScheduler;
        this.mUiScheduler = uiScheduler;
    }

    protected abstract Observable<ResultType> buildObservable(ParameterType parameter);

    public void execute(ParameterType parameter, Subscriber<ResultType> subscriber) {
        mSubscription.add(buildObservable(parameter)
                .subscribeOn(mJobScheduler)
                .observeOn(mUiScheduler)
                .subscribe(subscriber));
    }

    public void execute(Subscriber<ResultType> subscriber) {
        execute(null, subscriber);
    }

    public void unsubscribe() {
        mSubscription.clear();
    }
}


package com.swtch.switchmoviegrid.utils;

/**
 * Created by Sergey Yakimov on 12/08/15.
 */
public interface EndlessPagingListener {
    void onLoadPage(int pageIndex);
}

package com.swtch.switchmoviegrid.activities;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.swtch.switchmoviegrid.AppConstants;
import com.swtch.switchmoviegrid.MainApplication;
import com.swtch.switchmoviegrid.R;
import com.swtch.switchmoviegrid.model.Movie;
import com.swtch.switchmoviegrid.model.MoviesListCallback;
import com.swtch.switchmoviegrid.model.ServerManager;
import com.swtch.switchmoviegrid.utils.BlurTransformation;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


public class MovieDetailsActivity extends AppCompatActivity {

    private static final float BLUR_RADIUS = 25F;
    private static final int BACKGROUND_IMAGES_WIDTH = 342;
    private static final int BACKGROUND_IMAGES_HEIGHT = 484;

    private BlurTransformation mBlurTransformation;
    private Point mBackgroundImageTargetSize;

    private Toolbar mToolbar;

    private ImageView mBgndImageView;
    private ImageView mPosterImageView;

    private TextView mScoreTextView;
    private TextView mRatingTextView;
    private TextView mReleaseDateTextView;

    private TextView mInfoTitleTextView;
    private TextView mInfoOverviewTextView;

    private String mImdbId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);

        mToolbar = (Toolbar) findViewById(R.id.tool_bar);
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        mBlurTransformation = new BlurTransformation(this, BLUR_RADIUS);
        mBgndImageView = (ImageView) findViewById(R.id.bgnd_image);
        mBackgroundImageTargetSize = calculateBackgroundImageSizeCroppedToScreenAspectRatio(
                getWindowManager().getDefaultDisplay());
        updateBackground();

        mPosterImageView = (ImageView) findViewById(R.id.poster_image);
        mScoreTextView = (TextView) findViewById(R.id.score_label);
        mRatingTextView = (TextView) findViewById(R.id.rating_label);
        mReleaseDateTextView = (TextView) findViewById(R.id.release_date_label);
        mInfoTitleTextView = (TextView) findViewById(R.id.info_title_label);
        mInfoOverviewTextView = (TextView) findViewById(R.id.info_overview_label);
        updateInfo();
        loadMovieDetails();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_movie_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_browse) {
            openImdbPage();
            return true;
        }

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Info");
        alertDialog.setCancelable(false);
        alertDialog.setMessage("Press OK");
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog alert = alertDialog.create();
        alert.show();

        return super.onOptionsItemSelected(item);
    }

    private static Point calculateBackgroundImageSizeCroppedToScreenAspectRatio(Display display) {
        final Point screenSize = new Point();
        getSizeCompat(display, screenSize);
        int scaledWidth = (int) (((double) BACKGROUND_IMAGES_HEIGHT * screenSize.x) / screenSize.y);
        int croppedWidth = Math.min(scaledWidth, BACKGROUND_IMAGES_WIDTH);
        int scaledHeight = (int) (((double) BACKGROUND_IMAGES_WIDTH * screenSize.y) / screenSize.x);
        int croppedHeight = Math.min(scaledHeight, BACKGROUND_IMAGES_HEIGHT);
        return new Point(croppedWidth, croppedHeight);
    }

    @SuppressWarnings("deprecation")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private static void getSizeCompat(Display display, Point screenSize) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            display.getSize(screenSize);
        } else {
            screenSize.x = display.getWidth();
            screenSize.y = display.getHeight();
        }
    }

    private void updateBackground() {
        Movie m = MainApplication.getMovieDetails();

        Picasso.with(this).load(AppConstants.POSTER_PREFIX + m.getPosterPath())
                .resize(mBackgroundImageTargetSize.x, mBackgroundImageTargetSize.y)
                .centerCrop()
                .transform(mBlurTransformation)
                .into(mBgndImageView);
    }

    private void updateInfo() {
        Movie m = MainApplication.getMovieDetails();

        Calendar c = Calendar.getInstance();
        c.setTime(m.getReleaseDate());
        setTitle(m.getTitle() + " (" + c.get(Calendar.YEAR) + ")");

        try {
            Picasso p = Picasso.with(this);
            p.setLoggingEnabled(true);
            p.load(AppConstants.POSTER_PREFIX + m.getPosterPath())
                    .into(mPosterImageView);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mScoreTextView.setText(m.getVoteAvg());
        mRatingTextView.setText(m.isAdult() ? "R" : "G");

        DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, Locale.US);
        String formattedDate = df.format(m.getReleaseDate());
        mReleaseDateTextView.setText(formattedDate);

        mInfoTitleTextView.setText(m.getTitle() + " (" + c.get(Calendar.YEAR) + ")");
        mInfoOverviewTextView.setText(m.getOverview());
    }

    private void loadMovieDetails() {
        Movie m = MainApplication.getMovieDetails();
        try {
            ServerManager.getInstance().getMovieDetails(m.getId(), new MoviesListCallback() {
                @Override
                public void onGetItems(boolean success, int pageIndex, final List<Movie> list) {
                }

                @Override
                public void onGetItem(Movie movie) {
                    if (movie != null) {
                        mImdbId = movie.getImdbId();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openImdbPage() {
        if (mImdbId != null) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format(AppConstants.IMDB_URL, mImdbId)));
            startActivity(browserIntent);
        }
    }

}

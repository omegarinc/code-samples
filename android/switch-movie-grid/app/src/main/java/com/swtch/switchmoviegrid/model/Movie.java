package com.swtch.switchmoviegrid.model;

import android.util.Log;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Sergey Yakimov on 12/08/15.
 */
public class Movie {
    private String mId;
    private String mTitle;
    private String mOverview;
    private Date mReleaseDate;
    private String mPosterPath;
    private String mVoteAvg;
    private boolean mIsAdult;
    private String mImdbId;

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public String getOverview() {
        return mOverview;
    }

    public void setOverview(String overview) {
        this.mOverview = overview;
    }

    public Date getReleaseDate() {
        return mReleaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.mReleaseDate = releaseDate;
    }

    public String getPosterPath() {
        return mPosterPath;
    }

    public void setPosterPath(String posterPath) {
        this.mPosterPath = posterPath;
    }

    public String getVoteAvg() {
        return mVoteAvg;
    }

    public void setVoteAvg(String voteAvg) {
        this.mVoteAvg = voteAvg;
    }

    public boolean isAdult() {
        return mIsAdult;
    }

    public void setIsAdult(boolean isAdult) {
        this.mIsAdult = isAdult;
    }

    public String getImdbId() {
        return mImdbId;
    }

    public void setImdbId(String imdbId) {
        this.mImdbId = imdbId;
    }

    public static Movie getMovieFromJson(JSONObject o) {
        Movie m = new Movie();

        try {
            if (o.has("imdb_id")) m.setImdbId(o.getString("imdb_id"));

            if (o.has("adult")) m.setIsAdult(o.getBoolean("adult"));
            if (o.has("id")) m.setId(o.getString("id"));
            if (o.has("overview")) m.setOverview(o.getString("overview"));

            if (o.has("release_date")) {
                try {
                    Date date = dateFormat.parse(o.getString("release_date"));
                    m.setReleaseDate(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                    m.setReleaseDate(new Date());
                }
            }

            if (o.has("poster_path")) m.setPosterPath(o.getString("poster_path"));
            if (o.has("title")) m.setTitle(o.getString("title"));
            if (o.has("vote_average")) m.setVoteAvg(o.getString("vote_average"));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return m;
    }

}

package com.swtch.switchmoviegrid.activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.swtch.switchmoviegrid.AppConstants;
import com.swtch.switchmoviegrid.MainApplication;
import com.swtch.switchmoviegrid.R;
import com.swtch.switchmoviegrid.model.Movie;
import com.swtch.switchmoviegrid.model.MovieListAdapter;
import com.swtch.switchmoviegrid.model.MoviesListCallback;
import com.swtch.switchmoviegrid.model.ServerManager;
import com.swtch.switchmoviegrid.utils.EndlessPagingListener;
import com.swtch.switchmoviegrid.utils.EndlessScrollListener;

import java.util.HashMap;
import java.util.List;


public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, EndlessPagingListener {

    private final static int FIRST_PAGE = 1;

    private Toolbar mToolbar;

    private SwipeRefreshLayout mSwipeLayout;

    private GridView mMoviesGridView;
    private MovieListAdapter mMoviesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mToolbar);

        mMoviesGridView = (GridView) findViewById(R.id.movie_grid);
        setupGridSelectors();

        mSwipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        mSwipeLayout.setOnRefreshListener(this);
        mSwipeLayout.setColorSchemeResources(R.color.ColorPrimary);

        getLatestMovies(true, FIRST_PAGE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public void onRefresh() {
        getLatestMovies(true, FIRST_PAGE);
    }

    @Override
    public void onLoadPage(int pageIndex) {
        getLatestMovies(false, pageIndex);
    }

    private void getLatestMovies(final boolean resetAll, int pageIndex) {
        int page = mMoviesAdapter != null ? (mMoviesAdapter.getCount() / AppConstants.ITEMS_PER_PAGE) + 1 : FIRST_PAGE;
        if (resetAll) {
            page = FIRST_PAGE;
        }

        try {
            ServerManager.getInstance().getLatestMovies(page, new MoviesListCallback() {
                @Override
                public void onGetItems(boolean success, int pageIndex, final List<Movie> list) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeLayout.setRefreshing(false);

                            if (resetAll) {
                                if (mMoviesAdapter != null) {
                                    mMoviesAdapter.updateData(list);
                                } else {
                                    mMoviesAdapter = new MovieListAdapter(MainActivity.this, list);
                                    mMoviesGridView.setAdapter(mMoviesAdapter);
                                    mMoviesGridView.setOnScrollListener(new EndlessScrollListener(MainActivity.this));
                                }
                            } else {
                                mMoviesAdapter.addItems(list);
                            }
                        }
                    });
                }

                @Override
                public void onGetItem(Movie movie) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupGridSelectors() {
        mMoviesGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Movie m = mMoviesAdapter.getItem(position);

                if (m == null) {
                    return;
                }

                // just static, no db for now
                MainApplication.setMovieDetails(m);

                // goto details view
                Intent intent = new Intent(MainActivity.this, MovieDetailsActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });
    }
}

package com.swtch.switchmoviegrid;

/**
 * Created by Sergey Yakimov on 12/08/15.
 */
public class AppConstants {
    public static final String API_KEY = "ebea8cfca72fdff8d2624ad7bbf78e4c";
    public static final String API_NOW_PLAYING_PATH = "http://api.themoviedb.org/3/movie/now_playing?api_key=%s&page=%d";
    public static final String API_DETAILS_PATH = "http://api.themoviedb.org/3/movie/%s?api_key=%s";
    public static final String IMDB_URL = "http://www.imdb.com/title/%s/";

    public static final int ITEMS_PER_PAGE = 20;
    public static final String POSTER_PREFIX = "http://image.tmdb.org/t/p/w342";

}

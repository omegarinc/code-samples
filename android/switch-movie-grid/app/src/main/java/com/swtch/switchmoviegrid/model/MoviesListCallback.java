package com.swtch.switchmoviegrid.model;

import java.util.List;

/**
 * Created by Sergey Yakimov on 12/08/15.
 */
public abstract class MoviesListCallback {
    public abstract void onGetItems(boolean success, int pageIndex, List<Movie> list);
    public abstract void onGetItem(Movie movie);
}
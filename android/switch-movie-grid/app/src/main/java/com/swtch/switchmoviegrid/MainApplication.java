package com.swtch.switchmoviegrid;

import android.app.Application;
import android.content.Context;

import com.swtch.switchmoviegrid.model.Movie;

/**
 * Created by Sergey Yakimov on 12/08/15.
 */
public class MainApplication extends Application {
    private static Context sContext = null;

    private static Movie sMovieDetails;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();

        init();
    }

    public static Context getContext() {
        return sContext;
    }

    public void init() {
    }

    public static Movie getMovieDetails() {
        return sMovieDetails;
    }

    public static void setMovieDetails(Movie movieDetails) {
        sMovieDetails = movieDetails;
    }
}

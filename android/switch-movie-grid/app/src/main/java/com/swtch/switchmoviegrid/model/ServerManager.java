package com.swtch.switchmoviegrid.model;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.swtch.switchmoviegrid.AppConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergey Yakimov on 12/08/15.
 */
public class ServerManager {
    private static ServerManager sInstance;

    private final OkHttpClient mClient = new OkHttpClient();

    private ServerManager() {
    }

    public static ServerManager getInstance() {
        if (null == sInstance) {
            sInstance = new ServerManager();
        }
        return sInstance;
    }

    public void getLatestMovies(final int pageIndex, final MoviesListCallback mlc) throws Exception {
        Request request = new Request.Builder()
                .url(String.format(AppConstants.API_NOW_PLAYING_PATH, AppConstants.API_KEY, pageIndex))
                .build();

        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                e.printStackTrace();

                mlc.onGetItems(false, pageIndex, new ArrayList<Movie>());
            }

            @Override
            public void onResponse(Response response) {
                if (mlc == null) {
                    return;
                }

                if (!response.isSuccessful()) {
                    mlc.onGetItems(false, pageIndex, new ArrayList<Movie>());
                }

                try {
                    List<Movie> items = new ArrayList<Movie>();
                    String responseStr = response.body().string();
                    JSONObject object = new JSONObject(responseStr);
                    JSONArray resultsArray = object.getJSONArray("results");

                    if (resultsArray != null) {
                        for (int idx = 0; idx < resultsArray.length(); idx++) {
                            JSONObject movieObject = resultsArray.getJSONObject(idx);
                            Movie movie = Movie.getMovieFromJson(movieObject);
                            items.add(movie);
                        }
                    }

                    mlc.onGetItems(true, pageIndex, items);

                } catch (Exception e) {
                    e.printStackTrace();

                    mlc.onGetItems(false, pageIndex, new ArrayList<Movie>());
                }
            }
        });
    }

    public void getMovieDetails(final String movieId, final MoviesListCallback mlc) throws Exception {
        Request request = new Request.Builder()
                .url(String.format(AppConstants.API_DETAILS_PATH, movieId, AppConstants.API_KEY))
                .build();

        mClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Response response) {
                if (mlc == null) {
                    return;
                }

                if (!response.isSuccessful()) {
                    return;
                }

                try {
                    String responseStr = response.body().string();
                    JSONObject object = new JSONObject(responseStr);
                    Movie movie = Movie.getMovieFromJson(object);
                    mlc.onGetItem(movie);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}

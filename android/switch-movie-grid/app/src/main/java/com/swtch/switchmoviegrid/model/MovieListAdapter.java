package com.swtch.switchmoviegrid.model;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.swtch.switchmoviegrid.AppConstants;
import com.swtch.switchmoviegrid.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Sergey Yakimov on 12/08/15.
 */
public class MovieListAdapter extends BaseAdapter {

    private List<Movie> mData;
    private Context mContext;
    private LayoutInflater mInflater;

    public MovieListAdapter(Context ctx, List<Movie> data) {
        super();
        this.mData = data != null ? data : new ArrayList<Movie>();
        this.mContext = ctx;
        this.mInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Movie getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = mInflater.inflate(R.layout.item_grid, parent, false);

            holder.posterImageView = (ImageView) view.findViewById(R.id.poster_image);
            holder.nameTextView = (TextView) view.findViewById(R.id.name_label);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        Movie m = getItem(position);

        holder.nameTextView.setText(m.getTitle());

        try {
            Picasso p = Picasso.with(mContext);
            p.setLoggingEnabled(true);
            p.load(AppConstants.POSTER_PREFIX + m.getPosterPath())
             .into(holder.posterImageView);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    public void addItems(List<Movie> list) {
        for (Movie m: list) {
            mData.add(m);
        }
        notifyDataSetChanged();
    }

    public void updateData(List<Movie> list) {
        this.mData = list;

        notifyDataSetChanged();
    }

    private class ViewHolder {
        ImageView posterImageView;
        TextView nameTextView;
    }
}

package com.omegar.hitleapbotdemo;

import com.omegar.hitleapbotdemo.listeners.SearchResultLoadListener;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by User on 21.06.2016.
 */
public class GoogleCrawler {

    private static Pattern patternDomainName;
    private Matcher matcher;
    private static final String DOMAIN_NAME_PATTERN
            = "([a-zA-Z0-9]([a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9])?\\.)+[a-zA-Z]{2,6}";
    static {
        patternDomainName = Pattern.compile(DOMAIN_NAME_PATTERN);
    }

    public void loadSearchResults(String searchString, int allowedResultNumber, String neededDomainName, SearchResultLoadListener searchResultLoadListener) {

        GoogleCrawler obj = new GoogleCrawler();
        Set<String> result = obj.getDataFromGoogle(searchString, allowedResultNumber);
        for(String temp : result){
            System.out.println(temp);
        }
        System.out.println(result.size());
        searchResultLoadListener.onLoadingSuccessed(result,neededDomainName);
    }

    public String getDomainName(String url){

        String domainName = "";
        matcher = patternDomainName.matcher(url);
        if (matcher.find()) {
            domainName = matcher.group(0).toLowerCase().trim();
        }
        return domainName;

    }

    private Set<String> getDataFromGoogle(String query, int allowedResultNumber) {

        Set<String> result = new HashSet<>();
        String request = "https://www.google.com/search?q=" + query + "&num=" + String.valueOf(allowedResultNumber);
        System.out.println("Sending request..." + request);

        try {

            // need http protocol, set this as a Google bot agent :)
            Document doc = Jsoup
                    .connect(request)
                    .userAgent(
                            "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)")
                    .timeout(5000).get();

            // get all links
            Elements links = doc.select("a[href]");
            for (Element link : links) {

                String temp = link.attr("href");
                if(temp.startsWith("/url?q=")){
                    //use regex to get domain name
                    result.add(getDomainName(temp));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }
}

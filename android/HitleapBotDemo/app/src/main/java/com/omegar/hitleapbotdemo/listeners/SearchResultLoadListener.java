package com.omegar.hitleapbotdemo.listeners;

import java.util.List;
import java.util.Set;

/**
 * Created by User on 21.06.2016.
 */
public interface SearchResultLoadListener {

    void onLoadingSuccessed(Set<String> urls, String neededDomainName);
    void onLoadingFailed();

}

package com.omegar.hitleapbotdemo;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.omegar.hitleapbotdemo.listeners.SearchResultLoadListener;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements SearchResultLoadListener {

    public static final String TAG = MainActivity.class.getSimpleName();

    private EditText searchStringEditText;
    private EditText allowedResultNumberEditText;
    private EditText neededStringEditText;
    private WebView webview;

    private HashMap<String, String> searchWordsToNeededSite = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        searchStringEditText = (EditText) findViewById(R.id.search_string_et);
        allowedResultNumberEditText = (EditText) findViewById(R.id.allowed_result_number_et);
        neededStringEditText = (EditText) findViewById(R.id.needed_site_name_et);

        webview = (WebView) findViewById(R.id.page_view_wv);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                Log.i(TAG, "Webview with url " + url + " finished");
                makeAction(view);

            }
        });

        Button searchButton = (Button) findViewById(R.id.search_btn);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loadSearchResults(searchStringEditText.getText().toString(),
                        Integer.valueOf(allowedResultNumberEditText.getText().toString()));

                closeSoftKeyBoard();

            }
        });

        initializeExampleData();
    }

    public void initializeExampleData() {
        searchWordsToNeededSite.put("weather", "accuweather.com");
        searchWordsToNeededSite.put("weather ", "www.aviationweather.gov");
        searchWordsToNeededSite.put("translate", "yandex.ru");
        searchWordsToNeededSite.put("translate ", "rulate.ru");
        searchWordsToNeededSite.put("maps", "bing.com");
        searchWordsToNeededSite.put("maps ", "maps.me");
        searchWordsToNeededSite.put("news", "bbc.co.uk");
        searchWordsToNeededSite.put("news ", "nbcnews.com");
        searchWordsToNeededSite.put("calculator", "online-calculator.com");
        searchWordsToNeededSite.put("calculator ", "mortgagecalculator.org");
    }

    public void closeSoftKeyBoard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void makeAction(final WebView view) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        scrollVwToBottom(view);
                    }
                });

                try {
                    Thread.sleep(15000);
                } catch (InterruptedException e) {
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        clickOnLink(view);
                    }
                });

            }
        }).start();
    }

    public void scrollVwToBottom(WebView view) {
        view.scrollTo(0, view.getContentHeight());
        Log.i(TAG, "Scrolled to bottom");
    }

    public void clickOnLink(WebView view) {

        view.loadUrl("javascript:(function(){document.getElementsByTag('a.tagcloud-link').first().trigger('click');})()");
        Log.i("", "link programmatically clicked");

    }

    public void loadSearchResults(final String searchString, final int allowedResultNumber) {

        if (searchString.isEmpty()) {

            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (final Map.Entry<String, String> entry : searchWordsToNeededSite.entrySet()) {
                        try {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    searchStringEditText.setText(entry.getKey());
                                    neededStringEditText.setText(entry.getValue());
                                }
                            });

                            new GoogleCrawler().loadSearchResults(entry.getKey(), allowedResultNumber, entry.getValue(), MainActivity.this);
                            Thread.sleep(20000);
                        }catch (InterruptedException e){
                            e.printStackTrace();
                        }
                    }
                }
            }).start();

        } else {

            new Thread(new Runnable() {
                @Override
                public void run() {
                    String neededDomainName = neededStringEditText.getText().toString();
                    new GoogleCrawler().loadSearchResults(searchString, allowedResultNumber, neededDomainName, MainActivity.this);
                }
            }).start();
        }
    }


    @Override
    public void onLoadingSuccessed(Set<String> urls, String neededDomainName) {
        Log.i(TAG, "Search results loaded");

        final String neededUrl = getNeededStringFromSearchResult(urls, neededDomainName);
        if (neededUrl != null)
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    webview.loadUrl("http://" + neededUrl);
                }
            });

    }

    @Override
    public void onLoadingFailed() {
        Toast.makeText(MainActivity.this, "Search results loading failed", Toast.LENGTH_LONG).show();
    }


    public String getNeededStringFromSearchResult(Set<String> results, String neededDomainName) {

        for (String result : results) {
            if (result.toLowerCase().contains(neededDomainName.toLowerCase())) {
                return result;
            }
        }
        return null;
    }

}
